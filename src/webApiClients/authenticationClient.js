'use strict';
const superAgent = require('superagent');
const urlJoin = require('url-join');
const nodeUtil = require('@sportsdigita/platform-node-utility/index');
const exceptions = nodeUtil.exceptions;

/**
 * Client for interfacing with authentication web apis
 */
class AuthenticationClient {
    /**
     * Initialized the audioClient class
     * @param {TokenManager} tokenManager The tokenManager class containing connection details
     */
    constructor(tokenManager) {
        if (!tokenManager) {
            throw new exceptions.NullArgumentException(tokenManager);
        }

        this.tokenManager = tokenManager;
    }

    /**
     * Get the currently authenticated user's data.
     *
     * @returns user data
     * @memberof AuthenticationClient
     */
    async getUserInfo() {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/authentication/user');

        var response = await superAgent.get(url).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }

    /**
     * Get a token for a username and password
     *
     * @param {*} account
     * @returns token
     * @memberof AuthenticationClient
     */
    async signIn(account) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!audio) {
            throw new exceptions.NullArgumentException(audio);
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/authentication');

        var response = await superAgent.post(url, account);

        if (response && response.body && response.body.token) {
            this.tokenManager.webApiAuthorizationToken = response.body.token;
        }

        return response.body;
    }
}

module.exports = AuthenticationClient;

'use strict';
const superAgent = require('superagent');
const urlJoin = require('url-join');
const nodeUtil = require('@sportsdigita/platform-node-utility/index');
const exceptions = nodeUtil.exceptions;

/**
 * Functions used for interacting with the recording api
 */
class RecordingClient {
    /**
     * Initialized the recordingClient class
     * @param {TokenManager} tokenManager The tokenManager class containing connection details
     */
    constructor(tokenManager) {
        if (!tokenManager) {
            throw new exceptions.NullArgumentException(tokenManager);
        }

        this.tokenManager = tokenManager;
    }

    /**
     * Creates a recording record in Digideck system
     *
     * @param {object} recording the recording object to create.
     * @returns {Promise<object>} the recording object that was created.
     * @memberof RecordingClient
     */
    async createRecording(recording) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!recording) {
            throw new exceptions.NullArgumentException(recording);
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/recordings');

        var response = await superAgent.post(url, recording).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }

    /**
     * Sends a recording to gong
     *
     * @param {string} recordingId the id of the dd recording record requester wants to send to gong
     * @returns {Promise<object>} the gong recording that is returned from their api
     * @memberof RecordingClient
     */
    async sendRecordingToGong(recordingId) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!recordingId) {
            throw new exceptions.NullArgumentException(recordingId);
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), `/api/v1/recordings/${recordingId}/gong`);

        var response = await superAgent.post(url).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }

    /**
     * Update a recording record in the Digideck system.
     *
     * @param {string} recordingId id of the recording to update.
     * @param {object} recording the updated recording object
     * @returns {Promise<object>} the recording record that was updated
     * @memberof RecordingClient
     */
    async updateRecording(recordingId, recording) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!recordingId) {
            throw new exceptions.NullArgumentException(recordingId);
        }

        if (!recording) {
            throw new exceptions.NullArgumentException(recording);
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/recordings/', recordingId);

        var response = await superAgent.put(url, recording).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }

    /**
     * Gets a recording record in Digideck system.
     *
     * @param {string} recordingId The Id of the recording to get
     * @param {string} columns The list of columns to retrieve
     * @returns {Promise<object>} the recording record that was found
     */
    async getRecording(recordingId, columns) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!recordingId) {
            throw new exceptions.NullArgumentException(recordingId);
        }

        var queryString = '';

        if (columns) {
            queryString += '?columns=' + columns;
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/recordings', recordingId + queryString);

        var response = await superAgent.get(url).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }

    /**
     * Gets recording records in Digideck system.
     *
     * @param {object} filter The object used to filter recordings
     * @param {string} columns The list of columns to retrieve
     * @returns {Promise<object>} The recording record that was found.
     */
    async getRecordings(filter, columns) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!filter) {
            throw new exceptions.NullArgumentException(filter);
        }

        var queryString = '';

        if (columns) {
            queryString += '?columns=' + columns;
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/recordings' + queryString);

        var response = await superAgent.get(url, filter).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }
}

module.exports = RecordingClient;

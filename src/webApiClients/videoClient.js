'use strict';
const superAgent = require('superagent');
const urlJoin = require('url-join');
const nodeUtil = require('@sportsdigita/platform-node-utility/index');
const exceptions = nodeUtil.exceptions;

/**
 * Functions used for interacting with the videos api
 */
class videoClient {
    /**
     * Initialized the videoClient class
     * @param {TokenManager} tokenManager The tokenManager class containing connection details
     */
    constructor(tokenManager) {
        if (!tokenManager) {
            throw new exceptions.NullArgumentException(tokenManager);
        }

        this.tokenManager = tokenManager;
    }

    /**
     * Create creates an video in the web api
     * @param {object} video The video record to create
     * @param {boolean} preserveAuditData True to use the audit data from the file object, false to have the api override it with it's defaults
     * @returns {Promise<object>} The results of the video creation
     */
    async createVideo(video, preserveAuditData = false) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!video) {
            throw new exceptions.NullArgumentException(video);
        }

        var queryString = `?preserveAuditData=${preserveAuditData}`;

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), `/api/v1/videos/${queryString}`);

        var response = await superAgent.post(url, video).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }

    /**
     * Uploads an video file to the web api
     * @param {string} videoPath The path to the video file
     * @param {object} videoData The video metadata
     * @returns {Promise<object>} The results of the video upload
     */
    async uploadVideo(videoPath, videoData) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!videoPath) {
            throw new exceptions.NullArgumentException(videoPath);
        }

        if (!videoData) {
            throw new exceptions.NullArgumentException(videoData);
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/videos/transcoder/');

        var response = await superAgent.post(url).type('form').field('data', JSON.stringify(videoData)).attach('file', videoPath).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }

    /**
     * Gets a list of video records based on the criteria in the filter
     * @param {object} filter The search criteria
     * @param {string} columns The list of columns to fetch
     * @returns {Promise<object>} An object with the total number of records found and an array of the records.
     */
    async getVideos(filter, columns) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!filter) {
            throw new exceptions.NullArgumentException(filter);
        }

        var queryString = '';

        if (columns) {
            queryString += '?columns=' + columns;
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/videos/', queryString);

        var response = await superAgent.get(url, filter).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }

    /**
     * Gets a video record for the videoId
     * @param {string} videoId The id of the video to get
     * @param {string} columns The list of columns to fetch
     * @returns {Promise<object>} The found video record
     */
    async getVideo(videoId, columns) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!videoId) {
            throw new exceptions.NullArgumentException(videoId);
        }

        var queryString = '';

        if (columns) {
            queryString += '?columns=' + columns;
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/videos/', videoId + queryString);

        var response = await superAgent.get(url, videoId).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }

    /**
     * Updates the video object.
     * @param {string} videoId The id of the video to update
     * @param {object} videoData The video data that needs to be updated
     * @returns {Promise<object>} The updated video object
     */
    async updateVideo(videoId, videoData) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!videoId) {
            throw new exceptions.NullArgumentException(videoId);
        }

        if (!videoData) {
            throw new exceptions.NullArgumentException(videoData);
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/videos/', videoId);

        var response = await superAgent.put(url, videoData).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }
}

module.exports = videoClient;

'use strict';
const superAgent = require('superagent');
const urlJoin = require('url-join');
const nodeUtil = require('@sportsdigita/platform-node-utility/index');
const exceptions = nodeUtil.exceptions;

/**
 * Functions used for interacting with the InsightsSummaryJob api
 */
class InsightsSummaryJobClient {
    /**
     * Initialized the InsightsSummaryJobClient class
     * @param {TokenManager} tokenManager The tokenManager class containing connection details
     */
    constructor(tokenManager) {
        if (!tokenManager) {
            throw new exceptions.NullArgumentException(tokenManager);
        }

        this.tokenManager = tokenManager;
    }

    /**
     * Create a InsightsSummary job record in Digideck system.
     *
     * @param {string} insightsConfigurationId The insightsConfigurationId to create the InsightsSummary job for
     * @param {object} job The insightsSummary job object to create.
     * @param {object} queryStringOptions An object that will get converted into query string parameters.
     * @returns {Promise<object>} The insightsSummary job record that was created.
     */
    async createInsightsSummaryJob(insightsConfigurationId, job, queryStringOptions) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!job) {
            throw new exceptions.NullArgumentException(job);
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), `/api/v1/insightsConfiguration/${insightsConfigurationId}/insightssummaryjob`);

        if (queryStringOptions) {
            var queryString = '';
            Object.entries(queryStringOptions).forEach(([key, value]) => {
                if (queryString.length) {
                    queryString += '&';
                }
                queryString += `${key}=${value}`;
            });

            url += `?${queryString}`;
        }

        var response = await superAgent.post(url, job).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }
}

module.exports = InsightsSummaryJobClient;

'use strict';
const superAgent = require('superagent');
const urlJoin = require('url-join');
const nodeUtil = require('@sportsdigita/platform-node-utility/index');
const exceptions = nodeUtil.exceptions;

/**
 * Functions used for interacting with the decks api
 */
class deckClient {
    /**
     * Initialized the deckClient class
     * @param {TokenManager} tokenManager The tokenManager class containing connection details
     */
    constructor(tokenManager) {
        if (!tokenManager) {
            throw new exceptions.NullArgumentException(tokenManager);
        }

        this.tokenManager = tokenManager;
    }

    /**
     * Get deck data from web api
     * @param {string} deckId The id of the deck to get
     * @param {string} columns The columns to return
     * @returns {Promise<object>} The deck object
     */
    async getDeck(deckId, columns) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!deckId) {
            throw new exceptions.NullArgumentException(deckId);
        }

        var queryString = '';

        if (columns) {
            queryString += '?columns=' + columns;
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/decks/', deckId + queryString);

        var response = await superAgent.get(url).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }

    /**
     * Get deck data from web api by running a filter
     * @param {object} filter The search criteria used to find decks
     * @param {string} columns The columns to return
     * @returns {Promise<object>} The total records found and an array of decks found
     */
    async getDecks(filter, columns) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!filter) {
            throw new exceptions.NullArgumentException(filter);
        }

        var queryString = '';

        if (columns) {
            queryString += '?columns=' + columns;
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/decks/', queryString);

        var response = await superAgent.get(url, filter).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }

    /**
     * Add slides to deck
     * @param {string} deckId The id of the deck to add slides to.
     * @param {array} slides The slides to add to the deck
     * @returns {Promise<object>} An array of the slides that were created
     */
    async createDeckSlides(deckId, slides) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!deckId) {
            throw new exceptions.NullArgumentException(deckId);
        }

        if (!slides) {
            throw new exceptions.NullArgumentException(slides);
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/decks/', deckId, '/slides');

        var response = await superAgent.post(url, slides).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }

    /**
     * Updates the deck object.
     * @param {string} deckId The id of the deck to update
     * @param {object} deckData The deck data that needs to be updated
     * @returns {Promise<object>} The updated deck object
     */
    async updateDeck(deckId, deckData) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!deckId) {
            throw new exceptions.NullArgumentException(deckId);
        }

        if (!deckData) {
            throw new exceptions.NullArgumentException(deckData);
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/decks/', deckId);

        var response = await superAgent.put(url, deckData).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }

    /**
     * Get vanity URLs for all decks (V2)
     *
     * @param {*} filter - take and skip
     * @return {*} Array of vanity URLs and their deck URLs
     * @memberof deckClient
     */
    async getVanityUrls(filter) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!filter) {
            throw new exceptions.NullArgumentException(filter);
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v2/deckvanityurls');

        var response = await superAgent.get(url, filter).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }

    /**
     * get deck slides.
     * @param {string} deckId The id of the deck with slides to get
     * @param {object} filter for deck slides get
     * @returns {Promise<object>} array of deck slides
     */
    async getDeckSlides(deckId, filter) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!deckId) {
            throw new exceptions.NullArgumentException({ deckId });
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), `/api/v1/decks/${deckId}/slides`);

        var response = await superAgent.get(url, filter).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }

    /**
     * Updates deck slides.
     * @param {string} deckId The id of the deck with slides to update
     * @param {object} slides array of slides to update with _id on them
     * @returns {Promise<object>} The updated slides array
     */
    async updateDeckSlides(deckId, slides) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!deckId) {
            throw new exceptions.NullArgumentException({ deckId });
        }

        if (!slides?.length) {
            throw new exceptions.NullArgumentException({ slides });
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), `/api/v1/decks/${deckId}/slides`);

        var response = await superAgent.put(url, slides).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }

    /**
     * Updates a deck slide.
     * @param {string} deckId The id of the deck with slide
     * @param {string} slideId The id of the deck slide to update
     * @returns {Promise<object>} updated deck slide
     */
    async updateDeckSlide(deckId, slideId, slideUpdateObject) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!deckId) {
            throw new exceptions.NullArgumentException({ deckId });
        }

        if (!slideId) {
            throw new exceptions.NullArgumentException({ slideId });
        }

        if (!slideUpdateObject) {
            throw new exceptions.NullArgumentException({ slideUpdateObject });
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), `/api/v1/decks/${deckId}/slides/${slideId}`);

        var response = await superAgent.put(url, slideUpdateObject).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }
}

module.exports = deckClient;

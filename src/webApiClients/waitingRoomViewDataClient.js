'use strict';
const superAgent = require('superagent');
const urlJoin = require('url-join');
const nodeUtil = require('@sportsdigita/platform-node-utility/index');
const exceptions = nodeUtil.exceptions;

/**
 * The responsibility of this class is to interact with the waitingRoom view WebApi
 *
 * @class WaitingRoomViewDataClient
 */
class WaitingRoomViewDataClient {
    /**
     * Initialized the waitingRoomViewDataClient class
     * @param {TokenManager} tokenManager The tokenManager class containing connection details
     */
    constructor(tokenManager) {
        if (!tokenManager) {
            throw new exceptions.NullArgumentException(tokenManager);
        }

        this.tokenManager = tokenManager;
    }

    /**
     * Gets the waitingRoom view data for a waitingRoom
     *
     * @param {string} waitingRoomId The id of the waiting room
     * @returns {Promise<object>} Object containing all data for rendering a waiting room
     *
     * @memberof WaitingRoomViewDataClient
     */
    async getWaitingRoomViewData(waitingRoomId) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!waitingRoomId) {
            throw new exceptions.NullArgumentException(waitingRoomId);
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/waitingRoomView', waitingRoomId);

        var response = await superAgent.get(url).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }
}

module.exports = WaitingRoomViewDataClient;

'use strict';
const superAgent = require('superagent');
const urlJoin = require('url-join');
const nodeUtil = require('@sportsdigita/platform-node-utility/index');
const exceptions = nodeUtil.exceptions;

/**
 * Functions used for interacting with the masterstyles api
 */
class masterStyleClient {
    /**
     * Initialized the masterStyleClient class
     * @param {TokenManager} tokenManager The tokenManager class containing connection details
     */
    constructor(tokenManager) {
        if (!tokenManager) {
            throw new exceptions.NullArgumentException(tokenManager);
        }

        this.tokenManager = tokenManager;
    }

    /**
     * Create a masterStyle record in Digideck system.
     * @param {object} masterStyle The masterStyle object to create
     * @returns {Promise<object>} The masterStyle object that was created.
     */
    async createMasterStyle(masterStyle) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!masterStyle) {
            throw new exceptions.NullArgumentException(masterStyle);
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/masterstyles/');

        var response = await superAgent.post(url, masterStyle).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }
}

module.exports = masterStyleClient;

'use strict';
const superAgent = require('superagent');
const urlJoin = require('url-join');
const nodeUtil = require('@sportsdigita/platform-node-utility/index');
const exceptions = nodeUtil.exceptions;

/**
 * Functions used for interacting with the fonts api
 */
class fontClient {
    /**
     * Initialized the fontClient class
     * @param {TokenManager} tokenManager The tokenManager class containing connection details
     */
    constructor(tokenManager) {
        if (!tokenManager) {
            throw new exceptions.NullArgumentException(tokenManager);
        }

        this.tokenManager = tokenManager;
    }

    /**
     * Get fonts from Digideck system.
     *
     * @param {object} filter the object used to find fonts
     * @param {string} columns The columns to return
     * @returns {Promise<object>} The total records found and an array of fonts found
     * @memberof fontClient
     */
    async getFonts(filter, columns) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!filter) {
            throw new exceptions.NullArgumentException(filter);
        }

        var queryString = '';

        if (columns) {
            queryString += '?columns=' + columns;
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/fonts' + queryString);
        var response = await superAgent.get(url, filter).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }

    /**
     * Create a font record without uploading a font file.
     *
     * @param {object} font The font object to create.
     * @returns {Promise<object>} The font object that was created.
     * @memberof fontClient
     */
    async createFont(font) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!font) {
            throw new exceptions.NullArgumentException(font);
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/fonts/');

        var response = await superAgent.post(url, font).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }

    /**
     * Upload a font and its data to Digideck.
     *
     * @param {string} fontPath The path to the local font file
     * @param {object} fontData The font metadata
     * @returns {Promise<object>} The results of the font upload
     * @memberof fontClient
     */
    async uploadFont(fontPath, fontData) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!fontPath) {
            throw new exceptions.NullArgumentException(fontPath);
        }

        if (!fontData) {
            throw new exceptions.NullArgumentException(fontData);
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/fonts/transcoder/');

        var response = await superAgent.post(url).type('form').field('data', JSON.stringify(fontData)).attach('file', fontPath).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }
}

module.exports = fontClient;

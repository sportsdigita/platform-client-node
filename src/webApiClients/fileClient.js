'use strict';
const superAgent = require('superagent');
const urlJoin = require('url-join');
const nodeUtil = require('@sportsdigita/platform-node-utility/index');
const exceptions = nodeUtil.exceptions;

/**
 * Functions used to interact with the files api
 */
class fileClient {
    /**
     * Initialized the fileClient class
     * @param {TokenManager} tokenManager The tokenManager class containing connection details
     */
    constructor(tokenManager) {
        if (!tokenManager) {
            throw new exceptions.NullArgumentException(tokenManager);
        }

        this.tokenManager = tokenManager;
    }

    /**
     * Creates a file record without uploading a file.
     * @param {object} file The file object to create
     * @param {boolean} preserveAuditData True to use the audit data from the file object, false to have the api override it with it's defaults
     * @returns {Promise<object>} The file object that was created
     */
    async createFile(file, preserveAuditData = false) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!file) {
            throw new exceptions.NullArgumentException(file);
        }

        var queryString = `?preserveAuditData=${preserveAuditData}`;

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), `/api/v1/files/${queryString}`);

        var response = await superAgent.post(url, file).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }

    /**
     * Gets a list of file records based on the criteria in the filter
     * @param {object} filter The search criteria
     * @param {string} columns The list of columns to fetch
     * @returns {Promise<object>} An object with the total number of records found and an array of the records.
     */
    async getFiles(filter, columns) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!filter) {
            throw new exceptions.NullArgumentException(filter);
        }

        var queryString = '';

        if (columns) {
            queryString += '?columns=' + columns;
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/files/', queryString);

        var response = await superAgent.get(url, filter).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }

    /**
     * Updates the file object.
     * @param {string} fileId The id of the file to update
     * @param {object} fileData The file data that needs to be updated
     * @returns {Promise<object>} The updated file object
     */
    async updateFile(fileId, fileData) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!fileId) {
            throw new exceptions.NullArgumentException(fileId);
        }

        if (!fileData) {
            throw new exceptions.NullArgumentException(fileData);
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/files/', fileId);

        var response = await superAgent.put(url, fileData).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }
}

module.exports = fileClient;

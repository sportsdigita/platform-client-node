'use strict';
const superAgent = require('superagent');
const urlJoin = require('url-join');
const nodeUtil = require('@sportsdigita/platform-node-utility/index');
const exceptions = nodeUtil.exceptions;

/**
 * The responsibility of this class is to interact with the presentation view WebApi
 *
 * @class PresentationViewerClient
 */
class PresentationClient {
    /**
     * Initialized the presentationClient class
     * @param {TokenManager} tokenManager The tokenManager class containing connection details
     */
    constructor(tokenManager) {
        if (!tokenManager) {
            throw new exceptions.NullArgumentException(tokenManager);
        }

        this.tokenManager = tokenManager;
    }

    /**
     * Get presentations from Digideck system.
     *
     * @param {object} filter the object used to find presentations
     * @param {string} columns The columns to return
     * @returns {Promise<object>} The total records found and an array of presentations found
     */
    async getPresentations(filter, columns) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!filter) {
            throw new exceptions.NullArgumentException(filter);
        }

        var queryString = '';

        if (columns) {
            queryString += '?columns=' + columns;
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/presentations' + queryString);

        var response = await superAgent.get(url, filter).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());
        return response.body;
    }

    /**
     * Get a presentation by its ID
     *
     * @param {*} id
     * @param {*} columns
     * @returns
     * @memberof PresentationClient
     */
    async getPresentation(id, columns) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!id) {
            throw new exceptions.NullArgumentException(id);
        }

        var queryString = '';

        if (columns) {
            queryString += '?columns=' + columns;
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/presentations/' + id + queryString);

        var response = await superAgent.get(url).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());
        return response.body;
    }

    /**
     * Gets a summary of presentation stats
     * @param {string} groupBy The name of the property to group the stats by
     * @param {string} groupType Group type as predefined in the api. Current types are totalSeconds, averageSeconds, count, distinctCount, and sessionCount
     * @param {object} filter the object used to find presentations
     * @returns {Promise<object>} The total records found and an array of presentation summaries gathered
     */
    async getPresentationStats(groupBy, groupType, filter) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!groupBy) {
            throw new exceptions.NullArgumentException(groupBy);
        }

        if (!groupType) {
            throw new exceptions.NullArgumentException(groupType);
        }

        if (!filter) {
            throw new exceptions.NullArgumentException(filter);
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/presentationstats/', groupBy, groupType);

        var response = await superAgent.get(url, filter).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }

    /**
     * Update presentation properties
     * @param {string} presentationId The id of the presentation to be updated
     * @param {object} presentation An object containing the presentation properties to be updated
     * @returns {Promise<object>} The updated presentation object
     */
    async updatePresentation(presentationId, presentation) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!presentationId) {
            throw new exceptions.NullArgumentException(presentationId);
        }

        if (!presentation) {
            throw new exceptions.NullArgumentException(presentation);
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/presentations/', presentationId);
        var updatedPresentation = await superAgent.put(url, presentation).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return updatedPresentation.body;
    }

    /**
     * get presentation slides.
     * @param {string} presentationId The id of the presentation with slides to get
     * @param {object} filter for presentation slides get
     * @returns {Promise<object>} array of presentation slides
     */
    async getPresentationSlides(presentationId, filter) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!presentationId) {
            throw new exceptions.NullArgumentException({ presentationId });
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), `/api/v1/presentations/${presentationId}/slides`);

        var response = await superAgent.get(url, filter).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }

    /**
     * Update presentation slides
     * @param {string} presentationId The id of the presentation to be updated
     * @param {array} presentationSlides an array of presentation slides to be updated with _id
     * @returns {Promise<object>} The updated presentation slides array
     */
    async updatePresentationSlides(presentationId, presentationSlides) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!presentationId) {
            throw new exceptions.NullArgumentException({ presentationId });
        }

        if (!presentationSlides) {
            throw new exceptions.NullArgumentException({ presentationSlides });
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), `/api/v1/presentations/${presentationId}/slides`);
        var updatedPresentationSlides = await superAgent.put(url, presentationSlides).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return updatedPresentationSlides.body;
    }

    /**
     * Update presentation slide
     * @param {string} presentationId The id of the presentation to be updated
     * @param {string} presentationSlideId The id of the presentation slide to be updated
     * @param {array} presentationSlide the updated presentation slide object
     * @returns {Promise<object>} The updated presentation slide
     */
    async updatePresentationSlide(presentationId, presentationSlideId, presentationSlide) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!presentationId) {
            throw new exceptions.NullArgumentException({ presentationId });
        }

        if (!presentationSlide) {
            throw new exceptions.NullArgumentException({ presentationSlide });
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), `/api/v1/presentations/${presentationId}/slides/${presentationSlideId}`);
        var updatedPresentationSlide = await superAgent.put(url, presentationSlide).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return updatedPresentationSlide.body;
    }
}

module.exports = PresentationClient;

'use strict';
const superAgent = require('superagent');
const urlJoin = require('url-join');
const nodeUtil = require('@sportsdigita/platform-node-utility/index');
const exceptions = nodeUtil.exceptions;

/**
 * Functions used for interacting with the assets api
 */
class assetClient {
    /**
     * Initialized the assetClient class
     * @param {TokenManager} tokenManager The tokenManager class containing connection details
     */
    constructor(tokenManager) {
        if (!tokenManager) {
            throw new exceptions.NullArgumentException(tokenManager);
        }

        this.tokenManager = tokenManager;
    }

    /**
     * Get asset data from web api
     * @param {string} assetId The id of the asset to get
     * @param {string} columns The columns to return
     * @returns {Promise<object>} The asset object
     */
    async getAsset(assetId, columns) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!assetId) {
            throw new exceptions.NullArgumentException(assetId);
        }

        var queryString = '';

        if (columns) {
            queryString += '?columns=' + columns;
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/assets/', assetId + queryString);

        var response = await superAgent.get(url).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }

    /**
     * Get asset data from web api by running a filter
     * @param {object} filter The search criteria used to find assets
     * @param {string} columns The columns to return
     * @returns {Promise<object>} The total records found and an array of assets found
     */
    async getAssets(filter, columns) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!filter) {
            throw new exceptions.NullArgumentException(filter);
        }

        var queryString = '';

        if (columns) {
            queryString += '?columns=' + columns;
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/assets/', queryString);

        var response = await superAgent.get(url, filter).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }

    /**
     * Updates the asset object.
     * @param {string} assetId The id of the asset to update
     * @param {object} assetData The asset data that needs to be updated
     * @returns {Promise<object>} The updated asset object
     */
    async updateAsset(assetId, assetData) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!assetId) {
            throw new exceptions.NullArgumentException(assetId);
        }

        if (!assetData) {
            throw new exceptions.NullArgumentException(assetData);
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/assets/', assetId);

        var response = await superAgent.put(url, assetData).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }

    /**
     * Creates an asset record.
     * @param {object} asset The asset object to create
     * @returns {Promise<object>} The asset object that was created
     */
    async createAsset(asset) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!asset) {
            throw new exceptions.NullArgumentException(asset);
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/assets/');

        var response = await superAgent.post(url, asset).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }

    /**
     * Deletes an asset record.
     * @param {object} assetId The assetId to delete
     * @returns {Promise<object>} The asset object that was deleted
     */
    async deleteAsset(assetId) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!assetId) {
            throw new exceptions.NullArgumentException(assetId);
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/assets/', assetId);

        var response = await superAgent.delete(url).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }
}

module.exports = assetClient;

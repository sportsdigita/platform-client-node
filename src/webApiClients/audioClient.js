'use strict';
const superAgent = require('superagent');
const urlJoin = require('url-join');
const nodeUtil = require('@sportsdigita/platform-node-utility/index');
const exceptions = nodeUtil.exceptions;

/**
 * Functions used for interacting with the audio api
 */
class audioClient {
    /**
     * Initialized the audioClient class
     * @param {TokenManager} tokenManager The tokenManager class containing connection details
     */
    constructor(tokenManager) {
        if (!tokenManager) {
            throw new exceptions.NullArgumentException(tokenManager);
        }

        this.tokenManager = tokenManager;
    }

    /**
     * Create creates an audio in the web api
     * @param {object} audio The audio record to create
     * @param {boolean} preserveAuditData True to use the audit data from the file object, false to have the api override it with it's defaults
     * @returns {Promise<object>} The results of the audio creation
     */
    async createAudio(audio, preserveAuditData = false) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!audio) {
            throw new exceptions.NullArgumentException(audio);
        }

        var queryString = `?preserveAuditData=${preserveAuditData}`;

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), `/api/v1/audios/${queryString}`);

        var response = await superAgent.post(url, audio).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }

    /**
     * Uploads an audio file to the web api
     * @param {string} audioPath The path to the local audio file
     * @param {object} audioData The audio metadata
     * @returns {Promise<object>} The results of the audio upload
     */
    async uploadAudio(audioPath, audioData) {
        if (!this.tokenManager || !this.tokenManager.getWebApiBaseUrl()) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!audioPath) {
            throw new exceptions.NullArgumentException(audioPath);
        }

        if (!audioData) {
            throw new exceptions.NullArgumentException(audioData);
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/audios/transcoder/');

        var response = await superAgent.post(url).type('form').field('data', JSON.stringify(audioData)).attach('file', audioPath).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }

    /**
     * Gets a list of audio records based on the criteria in the filter
     * @param {object} filter The search criteria
     * @param {string} columns The list of columns to fetch
     * @returns {Promise<object>} An object with the total number of records found and an array of the records.
     */
    async getAudios(filter, columns) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!filter) {
            throw new exceptions.NullArgumentException(filter);
        }

        var queryString = '';

        if (columns) {
            queryString += '?columns=' + columns;
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/audios/', queryString);

        var response = await superAgent.get(url, filter).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }

    /**
     * Updates the audio object.
     * @param {string} audioId The id of the audio to update
     * @param {object} audioData The audio data that needs to be updated
     * @returns {Promise<object>} The updated audio object
     */
    async updateAudio(audioId, audioData) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!audioId) {
            throw new exceptions.NullArgumentException(audioId);
        }

        if (!audioData) {
            throw new exceptions.NullArgumentException(audioData);
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/audios/', audioId);

        var response = await superAgent.put(url, audioData).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }
}

module.exports = audioClient;

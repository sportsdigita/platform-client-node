'use strict';
const superAgent = require('superagent');
const urlJoin = require('url-join');
const nodeUtil = require('@sportsdigita/platform-node-utility/index');
const exceptions = nodeUtil.exceptions;

/**
 * Functions used for interacting with the masterStyleComponentStyles api
 */

class MasterStyleComponentStylesClient {
    /**
     * Initialized the MasterStyleComponentStylesClient class
     * @param {TokenManager} tokenManager The tokenManager class containing connection details
     */
    constructor(tokenManager) {
        if (!tokenManager) {
            throw new exceptions.NullArgumentException(tokenManager);
        }

        this.tokenManager = tokenManager;
    }

    /**
     * Get a masterstylecomponentstyle record
     *
     * @param {string} componentStyleId master style component style record id
     * @param {string} masterStyleId master style id
     * @returns {Promise<object>} The masterstylecomponentstyle record found
     */
    async getMasterStyleComponentStyle(componentStyleId, masterStyleId, filter = {}) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!componentStyleId || !masterStyleId) {
            throw new exceptions.NullArgumentException();
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), `/api/v1/masterstyles/${masterStyleId}/componentstyles/${componentStyleId}`);

        var response = await superAgent.get(url, filter).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }

    /**
     * Get masterstylecomponentstyles records
     *
     * @param {string} masterStyleId master style id
     * @param {object} filter the object used to filter masterstylecomponentstyles
     * @param {string} columns The columns to return
     * @returns {Promise<object>} The masterstylecomponentstyles records found
     */
    async getMasterStyleComponentStyles(masterStyleId, filter = {}, columns) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!filter || !masterStyleId) {
            throw new exceptions.NullArgumentException();
        }

        var queryString = '';

        if (columns) {
            queryString += '?columns=' + columns;
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), `/api/v1/masterstyles/${masterStyleId}/componentstyles${queryString}`);

        var response = await superAgent.get(url, filter).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }

    /**
     * Create a MasterStyleComponentStyle record
     *
     * @param {object} masterStyleId The masterStyleId the component style is associated with
     * @param {object} componentStyle The ComponentStyle object to create.
     * @returns {Promise<object>} The MasterStyleComponentStyle object that was created.
     */
    async createMasterStyleComponentStyle(masterStyleId, componentStyle) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!componentStyle) {
            throw new exceptions.NullArgumentException(componentStyle);
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), `/api/v1/masterstyles/${masterStyleId}/componentstyles`);

        var response = await superAgent.post(url, componentStyle).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }
}

module.exports = MasterStyleComponentStylesClient;

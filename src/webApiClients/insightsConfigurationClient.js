'use strict';
const superAgent = require('superagent');
const urlJoin = require('url-join');
const nodeUtil = require('@sportsdigita/platform-node-utility/index');
const exceptions = nodeUtil.exceptions;

/**
 * The responsibility of this class is to interact with the insightsConfiguration WebApi
 *
 * @class InsightsConfigurationClient
 */
class InsightsConfigurationClient {
    /**
     * Initialized the InsightsConfigurationClient class
     * @param {TokenManager} tokenManager The tokenManager class containing connection details
     */
    constructor(tokenManager) {
        if (!tokenManager) {
            throw new exceptions.NullArgumentException(tokenManager);
        }

        this.tokenManager = tokenManager;
    }

    /**
     * Gets the insightsConfiguration data from the web api
     *
     * @param {string} insightsConfigurationId The id of the insightsConfiguration
     * @param {string} columns The insightsConfiguration columns to retrieve
     * @returns {Promise<object>} The insightsConfiguration record
     *
     * @memberof InsightsConfigurationClient
     */
    async getInsightsConfiguration(insightsConfigurationId, columns) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!insightsConfigurationId) {
            throw new exceptions.NullArgumentException(insightsConfigurationId);
        }

        var queryString = '';

        if (columns) {
            queryString += '?columns=' + columns;
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/insightsConfigurations/', insightsConfigurationId + queryString);

        var response = await superAgent.get(url).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }

    /**
     * Get insightsConfigurations
     *
     * @param {object} filter the object used to find insightsConfigurations
     * @param {string} columns The columns to return
     * @returns {Promise<object>} The total records found and an array of insightsConfiguration found
     */
    async getInsightsConfigurations(filter, columns) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!filter) {
            throw new exceptions.NullArgumentException(filter);
        }

        var queryString = '';

        if (columns) {
            queryString += '?columns=' + columns;
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/insightsConfigurations/' + queryString);

        var response = await superAgent.get(url, filter).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());
        return response.body;
    }

    
}

module.exports = InsightsConfigurationClient;

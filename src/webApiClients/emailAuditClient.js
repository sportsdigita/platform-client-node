'use strict';
const superAgent = require('superagent');
const urlJoin = require('url-join');
const nodeUtil = require('@sportsdigita/platform-node-utility/index');
const exceptions = nodeUtil.exceptions;

/**
 * The responsibility of this class is to interact with the emailAudit view WebApi
 *
 * @class EmailAuditClient
 */
class EmailAuditClient {
    /**
     * Initialized the EmailAuditClient class
     * @param {TokenManager} tokenManager The tokenManager class containing connection details
     */
    constructor(tokenManager) {
        if (!tokenManager) {
            throw new exceptions.NullArgumentException(tokenManager);
        }

        this.tokenManager = tokenManager;
    }

    /**
     * Gets the emailAudit data for a email audit
     *
     * @param {string} emailAuditId The id of the email audit
     * @param {string} columns The email audit columns to retrieve
     * @returns {Promise<object>} The emailAudit record
     *
     * @memberof EmailAuditClient
     */
    async getEmailAudit(emailAuditId, columns) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!emailAuditId) {
            throw new exceptions.NullArgumentException(emailAuditId);
        }

        var queryString = '';

        if (columns) {
            queryString += '?columns=' + columns;
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/emailAudits', emailAuditId + queryString);

        var response = await superAgent.get(url).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }

    /**
     * Get email audits
     *
     * @param {object} filter the object used to find email audits
     * @param {string} columns The columns to return
     * @returns {Promise<object>} The total records found and an array of email audits found
     */
    async getEmailAudits(filter, columns) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!filter) {
            throw new exceptions.NullArgumentException(filter);
        }

        var queryString = '';

        if (columns) {
            queryString += '?columns=' + columns;
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/emailAudits' + queryString);

        var response = await superAgent.get(url, filter).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());
        return response.body;
    }

    /**
     * Create a emailAudit record.
     *
     * @param {object} emailAudit The emailAudit object to create.
     * @returns {Promise<object>} The emailAudit object that was created.
     */
    async createEmailAudit(emailAudit) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!emailAudit) {
            throw new exceptions.NullArgumentException(emailAudit);
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/emailAudits');

        var response = await superAgent.post(url, emailAudit).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }
}

module.exports = EmailAuditClient;

'use strict';
const superAgent = require('superagent');
const urlJoin = require('url-join');
const nodeUtil = require('@sportsdigita/platform-node-utility/index');
const exceptions = nodeUtil.exceptions;

/**
 * Functions used for interacting with the masterstyleforms api
 */
class masterStyleFormClient {
    /**
     * Initialized the masterStyleFormClient class
     * @param {TokenManager} tokenManager The tokenManager class containing connection details
     */
    constructor(tokenManager) {
        if (!tokenManager) {
            throw new exceptions.NullArgumentException(tokenManager);
        }

        this.tokenManager = tokenManager;
    }

    /**
     * Get masterstyleforms from Digideck system.
     *
     * @param {object} filter the object used to find masterstyleforms
     * @param {string} columns The columns to return
     * @returns {Promise<object>} The total records found and an array of masterstyleforms found
     */
    async getMasterStyleForms(filter, columns) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!filter) {
            throw new exceptions.NullArgumentException(filter);
        }

        var queryString = '';

        if (columns) {
            queryString += '?columns=' + columns;
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/masterstyleforms/' + queryString);

        var masterStyleForms = await superAgent.get(url, filter).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return masterStyleForms.body;
    }

    /**
     * Create a masterStyleForm record in Digideck system.
     *
     * @param {object} masterStyleForm The masterStyleForm object to create.
     * @returns {Promise<object>} The masterStyleForm object that was created.
     */
    async createMasterStyleForm(masterStyleForm) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!masterStyleForm) {
            throw new exceptions.NullArgumentException(masterStyleForm);
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/masterstyleforms/');

        var response = await superAgent.post(url, masterStyleForm).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }
}

module.exports = masterStyleFormClient;

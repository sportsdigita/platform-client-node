'use strict';
const superAgent = require('superagent');
const urlJoin = require('url-join');
const nodeUtil = require('@sportsdigita/platform-node-utility/index');
const exceptions = nodeUtil.exceptions;

/**
 * The responsibility of this class is to interact with the presentation view WebApi
 *
 * @class PresentationViewerClient
 */
class PresentationViewerClient {
    /**
     * Initialized the presentationViewerClient class
     * @param {TokenManager} tokenManager The tokenManager class containing connection details
     */
    constructor(tokenManager) {
        if (!tokenManager) {
            throw new exceptions.NullArgumentException(tokenManager);
        }

        this.tokenManager = tokenManager;
    }

    /**
     * Gets data for a master deck or presentation using the standard Digideck subdomain
     *
     * @param {string} subdomain The deck subdomain
     * @param {string} presentationName The name of the presentation. If not provided master deck will be returned.
     * @param {boolean} includeAllSlides True if all slides should be returned. False if only active slides should be returned. Default it false.
     * @param {boolean} includeTemplates True if templates should be included in the response. False if templates shouldn't be included. Default is false
     * @returns {Promise<object>} Object containing all data of the master deck or presentation
     *
     * @memberof PresentationViewerClient
     */
    async getPresentationViewer(subdomain, presentationName = '', includeAllSlides = false, includeTemplates = false) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!subdomain) {
            throw new exceptions.NullArgumentException(subdomain);
        }

        var queryString = '?subdomain=' + subdomain + '&allSlides=' + includeAllSlides + '&includeTemplates=' + includeTemplates;

        if (presentationName && presentationName.length > 0) {
            queryString += '&presentationName=' + presentationName;
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/presentationview' + queryString);

        var response = await superAgent.get(url).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }

    /**
     * Gets data for a master deck or presentation using a custom subdmain.
     *
     * @param {string} vanityUrl The vanity url set up on the deck.
     * @param {string} presentationName The name of the presentation. If not provided master deck will be returned.
     * @param {boolean} includeAllSlides True if all slides should be returned. False if only active slides should be returned. Default it false.
     * @param {boolean} includeTemplates True if templates should be included in the response. False if templates shouldn't be included. Default is false
     * @returns {Promise<object>} Object containing all data of the master deck or presentation
     *
     * @memberof PresentationViewerClient
     */
    async getPresentationViewerWithVanityUrl(vanityUrl, presentationName = '', includeAllSlides = false, includeTemplates = false) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!vanityUrl) {
            throw new exceptions.NullArgumentException(vanityUrl);
        }

        var queryString = "?subdomain=''&allSlides=" + includeAllSlides + '&includeTemplates=' + includeTemplates + '&vanityUrl=' + vanityUrl;

        if (presentationName && presentationName.length > 0) {
            queryString += '&presentationName=' + presentationName;
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/presentationview' + queryString);

        var response = await superAgent.get(url).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }
}

module.exports = PresentationViewerClient;

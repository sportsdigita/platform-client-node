'use strict';
const superAgent = require('superagent');
const urlJoin = require('url-join');
const nodeUtil = require('@sportsdigita/platform-node-utility/index');
const exceptions = nodeUtil.exceptions;

/**
 * Functions used for interacting with the masterStyleDocuments api
 */

class MasterStyleDocumentClient {
    /**
     * Initialized the masterStyleDocumentClient class
     * @param {TokenManager} tokenManager The tokenManager class containing connection details
     */
    constructor(tokenManager) {
        if (!tokenManager) {
            throw new exceptions.NullArgumentException(tokenManager);
        }

        this.tokenManager = tokenManager;
    }

    /**
     * Get masterstyledocuments from Digideck system.
     *
     * @param {object} filter the object used to find masterstyledocuments
     * @param {string} columns The columns to return
     * @returns {Promise<object>} The total records found and an array of masterstyledocuments found
     */
    async getMasterStyleDocuments(filter, columns) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!filter) {
            throw new exceptions.NullArgumentException(filter);
        }

        var queryString = '';

        if (columns) {
            queryString += '?columns=' + columns;
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/masterStyleDocuments/' + queryString);

        var response = await superAgent.get(url, filter).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }

    /**
     * Create a masterStyleDocument record in Digideck system.
     *
     * @param {object} masterStyleDocument The masterStyleDocument object to create.
     * @returns {Promise<object>} The masterStyleDocument object that was created.
     */
    async createMasterStyleDocument(masterStyleDocument) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!masterStyleDocument) {
            throw new exceptions.NullArgumentException(masterStyleDocument);
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/masterStyleDocuments');

        var response = await superAgent.post(url, masterStyleDocument).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }
}

module.exports = MasterStyleDocumentClient;

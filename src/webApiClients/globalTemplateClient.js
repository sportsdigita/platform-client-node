'use strict';
const superAgent = require('superagent');
const urlJoin = require('url-join');
const nodeUtil = require('@sportsdigita/platform-node-utility/index');
const exceptions = nodeUtil.exceptions;

/**
 * Functions used for interacting with the templates api
 */
class GlobalTemplateClient {
    /**
     * Initialized the globalTemplateClient class
     * @param {TokenManager} tokenManager The tokenManager class containing connection details
     */
    constructor(tokenManager) {
        if (!tokenManager) {
            throw new exceptions.NullArgumentException(tokenManager);
        }

        this.tokenManager = tokenManager;
    }

    /**
     * Get globalTemplates from Digideck system.
     *
     * @param {object} filter the object used to find globalTemplates
     * @param {string} columns The columns to return
     * @returns {Promise<object>} The total records found and an array of globalTemplates found
     */
    async getGlobalTemplates(filter, columns) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!filter) {
            throw new exceptions.NullArgumentException(filter);
        }

        var queryString = '';

        if (columns) {
            queryString += '?columns=' + columns;
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/globaltemplates/' + queryString);

        var response = await superAgent.get(url, filter).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }

    /**
     * Gets a global template
     *
     * @param {string} globalTemplateId The id of the global template
     * @param {string} columns The global template columns to retrieve
     * @returns {Promise<object>} The global template record
     *
     */
    async getGlobalTemplate(globalTemplateId, columns) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!globalTemplateId) {
            throw new exceptions.NullArgumentException(globalTemplateId);
        }

        var queryString = '';

        if (columns) {
            queryString += '?columns=' + columns;
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/globaltemplates', globalTemplateId + queryString);

        var response = await superAgent.get(url).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }
}

module.exports = GlobalTemplateClient;

'use strict';
const superAgent = require('superagent');
const urlJoin = require('url-join');
const nodeUtil = require('@sportsdigita/platform-node-utility/index');
const exceptions = nodeUtil.exceptions;

/**
 * Functions used for interacting with the users api
 */
class userClient {
    /**
     * Initialized the userClient class
     * @param {TokenManager} tokenManager The tokenManager class containing connection details
     */
    constructor(tokenManager) {
        if (!tokenManager) {
            throw new exceptions.NullArgumentException(tokenManager);
        }

        this.tokenManager = tokenManager;
    }

    /**
     * Get users from Digideck system.
     *
     * @param {object} filter the object used to find users
     * @param {string} columns The columns to return
     * @returns {Promise<object>} The total records found and an array of users found
     * @memberof userClient
     */
    async getUsers(filter, columns) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!filter) {
            throw new exceptions.NullArgumentException(filter);
        }

        var queryString = '';

        if (columns) {
            queryString += '?columns=' + columns;
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/users' + queryString);
        var response = await superAgent.get(url, filter).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }

    /**
     * Get a specific user from the Digideck
     *
     * @param {*} id The identifier for the user
     * @param {*} columns Which properties to return
     * @returns
     * @memberof userClient
     */
    async getUser(id, columns) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!id) {
            throw new exceptions.NullArgumentException(id);
        }

        var queryString = '';

        if (columns) {
            queryString += '?columns=' + columns;
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/users/' + id + queryString);
        var response = await superAgent.get(url, id).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }

    /**
     * Create a user record.
     *
     * @param {object} user The user object to create.
     * @returns {Promise<object>} The user object that was created.
     * @memberof userClient
     */
    async createUser(user) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!user) {
            throw new exceptions.NullArgumentException(user);
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/users/');

        var response = await superAgent.post(url, user).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }
}

module.exports = userClient;

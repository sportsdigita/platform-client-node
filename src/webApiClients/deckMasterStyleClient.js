'use strict';
const superAgent = require('superagent');
const urlJoin = require('url-join');
const nodeUtil = require('@sportsdigita/platform-node-utility/index');
const exceptions = nodeUtil.exceptions;

/**
 * Functions used for interacting with the deckMasterStyle api
 */
class deckMasterStyleClient {
    /**
     * Initialized the deckMasterStyleClient class
     * @param {TokenManager} tokenManager The tokenManager class containing connection details
     */
    constructor(tokenManager) {
        if (!tokenManager) {
            throw new exceptions.NullArgumentException(tokenManager);
        }

        this.tokenManager = tokenManager;
    }

    /**
     * Duplicates a master style
     *
     * @param {String} deckId The id of the deck the original master style is assigned to
     * @param {String} masterStyleId The id of the original master style
     * @param {Object} masterStyleOverrides The master style key/value's to override
     * @returns {Promise<Object>} The new master style.
     * @memberof deckMasterStyleClient
     */
    async duplicateMasterStyle(deckId, masterStyleId, masterStyleOverrides) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!deckId) {
            throw new exceptions.NullArgumentException(deckId);
        }

        if (!masterStyleId) {
            throw new exceptions.NullArgumentException(masterStyleId);
        }

        masterStyleOverrides = masterStyleOverrides || {};

        var url = urlJoin(`${this.tokenManager.getWebApiBaseUrl()}/api/v1/decks/${deckId}/masterstyles/${masterStyleId}`);

        var response = await superAgent.post(url, masterStyleOverrides).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }
}
module.exports = deckMasterStyleClient;

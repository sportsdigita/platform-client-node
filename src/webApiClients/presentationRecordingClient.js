'use strict';
const superAgent = require('superagent');
const urlJoin = require('url-join');
const nodeUtil = require('@sportsdigita/platform-node-utility/index');
const exceptions = nodeUtil.exceptions;

class PresentationRecordingClient {
    /**
     * Initialized the recordingClient class
     * @param {TokenManager} tokenManager The tokenManager class containing connection details
     */
    constructor(tokenManager) {
        if (!tokenManager) {
            throw new exceptions.NullArgumentException(tokenManager);
        }

        this.tokenManager = tokenManager;
    }

    /**
     * Create a presentation recording record in Digideck system.
     *
     * @param {string} deckId The Id of the deck the recording is for
     * @param {string} presentationId The Id of the presentation the recording is for
     * @param {object} recording The recording object to create.
     * @returns {Promise<object>} The recording record that was created.
     */
    async createPresentationRecording(deckId, presentationId, recording) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!deckId) {
            throw new exceptions.NullArgumentException(deckId);
        }

        if (!presentationId) {
            throw new exceptions.NullArgumentException(presentationId);
        }

        if (!recording) {
            throw new exceptions.NullArgumentException(recording);
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/decks/', deckId, '/presentations/', presentationId, '/recordings');

        var response = await superAgent.post(url, recording).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }

    /**
     * Update a presentation recording record in Digideck system.
     *
     * @param {string} deckId The Id of the deck the recording is for
     * @param {string} presentationId The Id of the presentation the recording is for
     * @param {string} recordingId The Id of the recording to update
     * @param {object} recording The updated recording object.
     * @returns {Promise<object>} The recording record that was updated.
     */
    async updatePresentationRecording(deckId, presentationId, recordingId, recording) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!deckId) {
            throw new exceptions.NullArgumentException(deckId);
        }

        if (!presentationId) {
            throw new exceptions.NullArgumentException(presentationId);
        }

        if (!recordingId) {
            throw new exceptions.NullArgumentException(recordingId);
        }

        if (!recording) {
            throw new exceptions.NullArgumentException(recording);
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/decks/', deckId, '/presentations/', presentationId, '/recordings/', recordingId);

        var response = await superAgent.put(url, recording).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }

    /**
     * Gets a presentation recording record in Digideck system.
     *
     * @param {string} deckId The Id of the deck the recording is for
     * @param {string} presentationId The Id of the presentation the recording is for
     * @param {string} recordingId The Id of the recording to get
     * @param {string} columns The list of columns to retrieve
     * @returns {Promise<object>} The recording record that was found.
     */
    async getPresentationRecording(deckId, presentationId, recordingId, columns) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!deckId) {
            throw new exceptions.NullArgumentException(deckId);
        }

        if (!presentationId) {
            throw new exceptions.NullArgumentException(presentationId);
        }

        if (!recordingId) {
            throw new exceptions.NullArgumentException(recordingId);
        }

        var queryString = '';

        if (columns) {
            queryString += '?columns=' + columns;
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/decks/', deckId, '/presentations/', presentationId, '/recordings/', recordingId + queryString);

        var response = await superAgent.get(url).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }

    /**
     * Gets presentation recording records in Digideck system.
     *
     * @param {string} deckId The Id of the deck the recording is for
     * @param {string} presentationId The Id of the presentation the recording is for
     * @param {object} filter The object used to filter recordings
     * @param {string} columns The list of columns to retrieve
     * @returns {Promise<object>} The recording record that was found.
     */
    async getPresentationRecordings(deckId, presentationId, filter, columns) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!deckId) {
            throw new exceptions.NullArgumentException(deckId);
        }

        if (!presentationId) {
            throw new exceptions.NullArgumentException(presentationId);
        }

        if (!filter) {
            throw new exceptions.NullArgumentException(filter);
        }

        var queryString = '';

        if (columns) {
            queryString += '?columns=' + columns;
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/decks/', deckId, '/presentations/', presentationId, '/recordings/' + queryString);

        var response = await superAgent.get(url, filter).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }
}

module.exports = PresentationRecordingClient;

'use strict';
const superAgent = require('superagent');
const urlJoin = require('url-join');
const nodeUtil = require('@sportsdigita/platform-node-utility/index');
const exceptions = nodeUtil.exceptions;

class ImpressionsClient {
    /**
     * Initialized the impressionClient class
     * @param {TokenManager} tokenManager The tokenManager class containing connection details
     */
    constructor(tokenManager) {
        if (!tokenManager) {
            throw new exceptions.NullArgumentException(tokenManager);
        }

        this.tokenManager = tokenManager;
    }

    /**
     * Gets a summary of impression data
     * @param {string} groupBy The name of the property to group the stats by
     * @param {string} groupType Group type as predefined in the api. Current types are totalSeconds, averageSeconds, count, distinctCount, and sessionCount
     * @param {object} filter the object used to find impressions
     * @returns {Promise<object>} The total records found and an array of impression summaries gathered
     */
    async getImpressionStats(groupBy, groupType, filter) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!groupBy) {
            throw new exceptions.NullArgumentException(groupBy);
        }

        if (!groupType) {
            throw new exceptions.NullArgumentException(groupType);
        }

        if (!filter) {
            throw new exceptions.NullArgumentException(filter);
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/impressionstats/', groupBy, groupType);

        var response = await superAgent.get(url, filter).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }
}

module.exports = ImpressionsClient;

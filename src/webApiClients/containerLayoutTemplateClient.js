'use strict';
const superAgent = require('superagent');
const urlJoin = require('url-join');
const nodeUtil = require('@sportsdigita/platform-node-utility/index');
const exceptions = nodeUtil.exceptions;

/**
 * Functions used for interacting with the templates api
 */
class ContainerLayoutTemplateClient {
    /**
     * Initialized the ContainerLayoutTemplateClient class
     * @param {TokenManager} tokenManager The tokenManager class containing connection details
     */
    constructor(tokenManager) {
        if (!tokenManager) {
            throw new exceptions.NullArgumentException(tokenManager);
        }

        this.tokenManager = tokenManager;
    }

    /**
     * Get ContainerLayoutTemplates from Digideck system.
     *
     * @param {object} filter the object used to find ContainerLayoutTemplates
     * @param {string} columns The columns to return
     * @returns {Promise<object>} The total records found and an array of ContainerLayoutTemplates found
     */
    async getContainerLayoutTemplates(filter, columns) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!filter) {
            throw new exceptions.NullArgumentException(filter);
        }

        var queryString = '';

        if (columns) {
            queryString += '?columns=' + columns;
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/containerlayouttemplates/' + queryString);

        var response = await superAgent.get(url, filter).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }
}

module.exports = ContainerLayoutTemplateClient;

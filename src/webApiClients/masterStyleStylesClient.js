'use strict';
const superAgent = require('superagent');
const nodeUtil = require('@sportsdigita/platform-node-utility/index');
const exceptions = nodeUtil.exceptions;
const urlJoin = require('url-join');

/**
 * Functions for interacting with MasterStyleStyles api
 */
class masterStyleStylesClient {
    /**
     * Initialized the masterStyleClient class
     * @param {TokenManager} tokenManager The tokenManager class containing connection details
     */
    constructor(tokenManager) {
        if (!tokenManager) {
            throw new exceptions.NullArgumentException(tokenManager);
        }

        this.tokenManager = tokenManager;
    }

    /**
     * Gets a list of MasterStyleStyles based on the criteria in the filter.
     * @param {string} deckId The deckId to find masterStyleStyles for
     * @param {string} masterStyleId  The masterStyleId to find masterStyleStyles for
     * @param {object} filter Filter for querying masterStyleStyles
     * @returns {Promise<object>} Object containing the total number of records found, and an array of the records found
     */
    async getMasterStyleStyles(deckId, masterStyleId, filter) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!deckId) {
            throw new exceptions.NullArgumentException(deckId);
        }

        if (!masterStyleId) {
            throw new exceptions.NullArgumentException(masterStyleId);
        }

        if (!filter) {
            throw new exceptions.NullArgumentException(filter);
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/decks/', deckId, '/masterstyles/', masterStyleId, '/styles/?filter=' + JSON.stringify(filter));

        var masterStyleStyles = await superAgent.get(url).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return masterStyleStyles.body;
    }

    /**
     * Saves a new masterStyleStyle
     * @param {string} deckId The deckId to find masterStyleStyles for
     * @param {string} masterStyleId  The masterStyleId to find masterStyleStyles for
     * @param {object} masterStyleStyle The masterStyleStyle object to save
     * @returns {Promise<object>} The created masterStyleStyle
     */
    async createMasterStyleStyle(deckId, masterStyleId, masterStyleStyle) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!deckId) {
            throw new exceptions.NullArgumentException(deckId);
        }

        if (!masterStyleId) {
            throw new exceptions.NullArgumentException(masterStyleId);
        }

        if (!masterStyleStyle) {
            throw new exceptions.NullArgumentException(masterStyleStyle);
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/decks/', deckId, '/masterstyles/', masterStyleId, '/styles/');

        var response = await superAgent.post(url, masterStyleStyle).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }
}

module.exports = masterStyleStylesClient;

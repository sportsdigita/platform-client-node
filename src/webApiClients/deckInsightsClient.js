'use strict';
const superAgent = require('superagent');
const urlJoin = require('url-join');
const nodeUtil = require('@sportsdigita/platform-node-utility/index');
const exceptions = nodeUtil.exceptions;

/**
 * Functions used to interact with the deck insights api
 */
class deckInsightsClient {
    /**
     * Initialized the deckInsightsClient class
     * @param {TokenManager} tokenManager The tokenManager class containing connection details
     */
    constructor(tokenManager) {
        if (!tokenManager) {
            throw new exceptions.NullArgumentException(tokenManager);
        }

        this.tokenManager = tokenManager;
    }

    /**
     * Creates a deckInsight record.
     * @param {string} deckId The deckId to create the deckInsight for
     * @param {object} deckInsight The deckInsight object to create
     * @returns {Promise<object>} The deckInsight object that was created
     */
    async createDeckInsight(deckId, deckInsight) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!deckInsight) {
            throw new exceptions.NullArgumentException(deckInsight);
        }

        if(!deckId){
            throw new exceptions.NullArgumentException(deckId);
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), `/api/v1/decks/${deckId}/insights`);

        var response = await superAgent.post(url, deckInsight).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }

}

module.exports = deckInsightsClient;

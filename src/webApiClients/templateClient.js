'use strict';
const superAgent = require('superagent');
const urlJoin = require('url-join');
const nodeUtil = require('@sportsdigita/platform-node-utility/index');
const exceptions = nodeUtil.exceptions;

/**
 * Functions used for interacting with the templates api
 */
class templateClient {
    /**
     * Initialized the templateClient class
     * @param {TokenManager} tokenManager The tokenManager class containing connection details
     */
    constructor(tokenManager) {
        if (!tokenManager) {
            throw new exceptions.NullArgumentException(tokenManager);
        }

        this.tokenManager = tokenManager;
    }

    /**
     * Create a template record in Digideck system.
     *
     * @param {object} template The template object to create.
     * @returns {Promise<object>} The template that was created.
     */
    async createTemplate(template) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!template) {
            throw new exceptions.NullArgumentException(template);
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/templates/');

        var response = await superAgent.post(url, template).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }
}

module.exports = templateClient;

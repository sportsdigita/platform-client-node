'use strict';
const superAgent = require('superagent');
const urlJoin = require('url-join');
const nodeUtil = require('@sportsdigita/platform-node-utility/index');
const exceptions = nodeUtil.exceptions;

/**
 * Functions used for interacting with the deckComponentVersions api
 */
class deckComponentVersionsClient {
    /**
     * Initialized the deckComponentVersionsClient class
     * @param {TokenManager} tokenManager The tokenManager class containing connection details
     */
    constructor(tokenManager) {
        if (!tokenManager) {
            throw new exceptions.NullArgumentException(tokenManager);
        }

        this.tokenManager = tokenManager;
    }

    /**
     * Adds components to the deck componentRegistry
     * @param {String} deckId The id of the deck for which to update the componentRegistry
     * @param {[Object]} components An array of components to add to the registry
     * @returns {Promise<object>} The deck that was updated
     */
    async addComponentsToDeckComponentRegistry(deckId, components) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!deckId) {
            throw new exceptions.NullArgumentException(deckId);
        }

        if (!components) {
            throw new exceptions.NullArgumentException(components);
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), `/api/v1/decks/${deckId}/components/registry`);

        var response = await superAgent.post(url, components).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }
}

module.exports = deckComponentVersionsClient;

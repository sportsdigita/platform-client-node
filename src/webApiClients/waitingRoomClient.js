'use strict';
const superAgent = require('superagent');
const urlJoin = require('url-join');
const nodeUtil = require('@sportsdigita/platform-node-utility/index');
const exceptions = nodeUtil.exceptions;

/**
 * The responsibility of this class is to interact with the waitingRoom view WebApi
 *
 * @class WaitingRoomClient
 */
class WaitingRoomClient {
    /**
     * Initialized the waitingRoomClient class
     * @param {TokenManager} tokenManager The tokenManager class containing connection details
     */
    constructor(tokenManager) {
        if (!tokenManager) {
            throw new exceptions.NullArgumentException(tokenManager);
        }

        this.tokenManager = tokenManager;
    }

    /**
     * Gets the waitingRoom data for a waitingRoom
     *
     * @param {string} waitingRoomId The id of the waiting room
     * @param {string} columns The waiting room columns to retrieve
     * @returns {Promise<object>} The waitingRoom record
     *
     * @memberof WaitingRoomClient
     */
    async getWaitingRoom(waitingRoomId, columns) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!waitingRoomId) {
            throw new exceptions.NullArgumentException(waitingRoomId);
        }

        var queryString = '';

        if (columns) {
            queryString += '?columns=' + columns;
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/waitingRooms', waitingRoomId + queryString);

        var response = await superAgent.get(url).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }

    /**
     * Get waiting rooms
     *
     * @param {object} filter the object used to find waitingRooms
     * @param {string} columns The columns to return
     * @returns {Promise<object>} The total records found and an array of waitingRooms found
     */
    async getWaitingRooms(filter, columns) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!filter) {
            throw new exceptions.NullArgumentException(filter);
        }

        var queryString = '';

        if (columns) {
            queryString += '?columns=' + columns;
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/waitingRooms' + queryString);

        var response = await superAgent.get(url, filter).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());
        return response.body;
    }

    /**
     * Create a waitingRoom record.
     *
     * @param {object} waitingRoom The waitingRoom object to create.
     * @returns {Promise<object>} The waitingRoom object that was created.
     */
    async createWaitingRoom(waitingRoom) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!waitingRoom) {
            throw new exceptions.NullArgumentException(waitingRoom);
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/waitingRooms');

        var response = await superAgent.post(url, waitingRoom).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }

    /**
     * Updates the waitingRoom object.
     * @param {string} waitingRoomId The id of the waitingRoom to update
     * @param {object} waitingRoomData The waitingRoom data that needs to be updated
     * @returns {Promise<object>} The updated waitingRoom object
     */
    async updateWaitingRoom(waitingRoomId, waitingRoomData) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!waitingRoomId) {
            throw new exceptions.NullArgumentException(waitingRoomId);
        }

        if (!waitingRoomData) {
            throw new exceptions.NullArgumentException(waitingRoomData);
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/waitingRooms/', waitingRoomId);

        var response = await superAgent.put(url, waitingRoomData).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }

    /**
     * Deletes a waitingRoom
     *
     * @param {string} waitingRoomId The id of the waiting room to delete
     * @returns {Promise<object>} The deleted waitingRoom record
     *
     * @memberof WaitingRoomClient
     */
    async deleteWaitingRoom(waitingRoomId) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!waitingRoomId) {
            throw new exceptions.NullArgumentException(waitingRoomId);
        }

        var url = urlJoin(this.tokenManager.getWebApiBaseUrl(), '/api/v1/waitingRooms', waitingRoomId);

        var response = await superAgent.delete(url).set('Authorization', this.tokenManager.getWebApiAuthorizationHeader());

        return response.body;
    }
}

module.exports = WaitingRoomClient;

'use strict';

const superAgent = require('superagent');
const urlJoin = require('url-join');
const nodeUtil = require('@sportsdigita/platform-node-utility/index');
const exceptions = nodeUtil.exceptions;

/**
 * Functions used for interacting with the deck viewer api
 */
class DeckViewerClient {
    /**
     * Initialized the deckViewerClient class
     * @param {TokenManager} tokenManager The tokenManager class containing connection details
     */
    constructor(tokenManager) {
        if (!tokenManager) {
            throw new exceptions.NullArgumentException(tokenManager);
        }

        this.tokenManager = tokenManager;
    }

    /**
     * Generate HTML based on deck or presentation data
     *
     * @param {object} data The deck or presentation data to generate the html for
     * @returns {Promise<object>} The generated html
     * @memberof DeckViewerClient
     */
    async generateDocument(data) {
        if (!this.tokenManager) {
            throw new exceptions.NullArgumentException(this.tokenManager);
        }

        if (!data) {
            throw new exceptions.NullArgumentException(data);
        }

        var url = urlJoin(this.tokenManager.getDeckViewerBaseUrl(), '/api/v1/documents');

        var response = await superAgent.post(url, data);

        return response.body;
    }
}

module.exports = DeckViewerClient;

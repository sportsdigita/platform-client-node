'use strict';

class TokenManager {
    configureWebApiTokenManager(baseUrl, authorizationToken) {
        if (!baseUrl) {
            throw 'Invalid baseUrl: ' + baseUrl;
        }

        this.webApiBaseUrl = baseUrl;
        this.webApiAuthorizationToken = authorizationToken;
    }

    getWebApiBaseUrl() {
        if (!this.webApiBaseUrl) {
            throw 'Web Api Base url not set';
        }
        return this.webApiBaseUrl;
    }

    getWebApiAuthorizationToken() {
        if (!this.webApiAuthorizationToken) {
            throw 'Web Api Authorization token not set';
        }
        return this.webApiAuthorizationToken;
    }

    getWebApiAuthorizationHeader() {
        if (!this.webApiAuthorizationToken) {
            throw 'Authorization token not set';
        }
        return 'Bearer ' + this.webApiAuthorizationToken;
    }

    configureDeckViewerTokenManager(baseUrl, authorizationToken) {
        if (!baseUrl) {
            throw 'Invalid baseUrl: ' + baseUrl;
        }

        this.deckViewerBaseUrl = baseUrl;
        this.deckViewerAuthorizationToken = authorizationToken;
    }

    getDeckViewerBaseUrl() {
        if (!this.deckViewerBaseUrl) {
            throw 'Deck Viewer Base url not set';
        }
        return this.deckViewerBaseUrl;
    }

    getDeckViewerAuthorizationToken() {
        if (!this.deckViewerAuthorizationToken) {
            throw 'Web Api Authorization token not set';
        }
        return this.deckViewerAuthorizationToken;
    }

    getDeckViewerAuthorizationHeader() {
        if (!this.deckViewerAuthorizationToken) {
            throw 'Authorization token not set';
        }
        return 'Bearer ' + this.deckViewerAuthorizationToken;
    }
}

module.exports = TokenManager;

const chai = require('chai');
const assert = chai.assert;
const nock = require('nock');
const deepCopy = require('deepcopy');
const tokenManager = new (require('../src/tokenManager'))();
const clients = require('../index');
const testUtil = require('./testUtil');

describe('deckViewerClient', function () {
    this.afterEach(function () {
        nock.cleanAll();
    });

    it('should get a deck html content', async function () {
        var generateDocumentNock = nock(testUtil.deckViewerBaseUrl)
            .post('/api/v1/documents')
            .reply(200, function (uri, requestBody) {
                return { content: '<html><body><div>Hello World</div></body></html>' };
            });

        tokenManager.configureDeckViewerTokenManager(testUtil.deckViewerBaseUrl);
        var deckViewerClient = new clients.DeckViewerClient(tokenManager);
        var response = await deckViewerClient.generateDocument({ slides: [{ _id: 'slide1' }, { _id: 'slide2' }], masterStyle: { _id: 'masterStyleId' } });

        assert.equal(response.content, '<html><body><div>Hello World</div></body></html>');
    });
});

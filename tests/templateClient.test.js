const chai = require('chai');
const assert = chai.assert;
const nock = require('nock');
const deepCopy = require('deepcopy');
const tokenManager = new (require('../src/tokenManager'))();
const clients = require('../index');
const testUtil = require('./testUtil');

describe('templateClient', function () {
    this.afterEach(function () {
        nock.cleanAll();
    });

    var templateRecord = {
        content: 'content',
        deckId: 'deckId',
        nestingLevel: -1
    };

    it('should create a template', async function () {
        var createTemplateNock = nock(testUtil.webApiBaseUrl)
            .post('/api/v1/templates/')
            .reply(200, function (uri, requestBody) {
                requestBody._id = 'id';
                return requestBody;
            });

        tokenManager.configureWebApiTokenManager(testUtil.webApiBaseUrl, 'token');
        var templateClient = new clients.TemplateClient(tokenManager);

        var response = await templateClient.createTemplate(templateRecord);

        assert.equal(response._id, 'id');
        assert.equal(response.content, templateRecord.content);
        assert.equal(response.deckId, templateRecord.deckId);
        assert.equal(response.nestingLevel, templateRecord.nestingLevel);
    });
});

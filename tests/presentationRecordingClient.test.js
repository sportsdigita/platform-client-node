const chai = require('chai');
const assert = chai.assert;
const nock = require('nock');
const deepCopy = require('deepcopy');
const tokenManager = new (require('../src/tokenManager'))();
const clients = require('../index');
const testUtil = require('./testUtil');

describe('presentationRecordingClient', function () {
    this.afterEach(function () {
        nock.cleanAll();
    });

    var recordingRecord = {
        deckId: 'deckId',
        presentationId: 'presentationId',
        title: 'myRecording',
        roomName: 'myRoom'
    };

    it('should create a presentation recording', async function () {
        var createNock = nock(testUtil.webApiBaseUrl)
            .post('/api/v1/decks/deckId/presentations/presentationId/recordings')
            .reply(200, function (uri, requestBody) {
                requestBody._id = 'id';
                return requestBody;
            });

        tokenManager.configureWebApiTokenManager(testUtil.webApiBaseUrl, 'token');
        var recordingClient = new clients.PresentationRecordingClient(tokenManager);

        var response = await recordingClient.createPresentationRecording('deckId', 'presentationId', recordingRecord);

        assert.equal(response._id, 'id');
        assert.equal(response.deckId, recordingRecord.deckId);
        assert.equal(response.presentationId, recordingRecord.presentationId);
        assert.equal(response.title, recordingRecord.title);
        assert.equal(response.roomName, recordingRecord.roomName);
    });

    it('should update a presentation recording', async function () {
        var updateNock = nock(testUtil.webApiBaseUrl)
            .put('/api/v1/decks/deckId/presentations/presentationId/recordings/id')
            .reply(200, function (uri, requestBody) {
                var updatedRecording = deepCopy(recordingRecord);
                updatedRecording._id = 'id';
                updatedRecording.videoId = 'videoId';
                return updatedRecording;
            });

        tokenManager.configureWebApiTokenManager(testUtil.webApiBaseUrl, 'token');
        var recordingClient = new clients.PresentationRecordingClient(tokenManager);

        var response = await recordingClient.updatePresentationRecording('deckId', 'presentationId', 'id', { videoId: 'videoId' });

        assert.equal(response.videoId, 'videoId');
        assert.equal(response._id, 'id');
        assert.equal(response.deckId, recordingRecord.deckId);
        assert.equal(response.presentationId, recordingRecord.presentationId);
        assert.equal(response.title, recordingRecord.title);
        assert.equal(response.roomName, recordingRecord.roomName);
    });

    it('should get a presentation recording', async function () {
        var getNock = nock(testUtil.webApiBaseUrl)
            .get('/api/v1/decks/deckId/presentations/presentationId/recordings/id')
            .reply(200, function (uri, requestBody) {
                var response = deepCopy(recordingRecord);
                response._id = 'id';
                return response;
            });

        tokenManager.configureWebApiTokenManager(testUtil.webApiBaseUrl, 'token');
        var recordingClient = new clients.PresentationRecordingClient(tokenManager);
        var recording = await recordingClient.getPresentationRecording('deckId', 'presentationId', 'id');

        assert.equal(recording._id, 'id');
        assert.equal(recording.deckId, recordingRecord.deckId);
        assert.equal(recording.presentationId, recordingRecord.presentationId);
        assert.equal(recording.title, recordingRecord.title);
        assert.equal(recording.roomName, recordingRecord.roomName);
    });

    it('should get recordings based on a filter', async function () {
        var getNock = nock(testUtil.webApiBaseUrl)
            .get('/api/v1/decks/deckId/presentations/presentationId/recordings' + '?columns=title&presentationId=presentationId')
            .reply(200, function (uri, requestBody) {
                return { total: 1, results: [{ _id: 'id', title: 'myRecording' }] };
            });

        tokenManager.configureWebApiTokenManager(testUtil.webApiBaseUrl, 'token');
        var recordingClient = new clients.PresentationRecordingClient(tokenManager);
        var filter = { presentationId: 'presentationId' };
        var recordings = await recordingClient.getPresentationRecordings('deckId', 'presentationId', filter, 'title');
        assert.equal(recordings.total, 1);
        assert.equal(recordings.results[0]._id, 'id');
        assert.equal(recordings.results[0].title, 'myRecording');
    });
});

const chai = require('chai');
const assert = chai.assert;
const nock = require('nock');
const tokenManager = new (require('../src/tokenManager'))();
const clients = require('../index');
const testUtil = require('./testUtil');

describe('masterStyleFormClient', function () {
    this.afterEach(function () {
        nock.cleanAll();
    });

    it('should create a masterStyleForm', async function () {
        var masterStyleForm = {
            name: 'testMSF',
            deckId: '1234',
            masterStyleId: '2345'
        };

        var createMasterStyleFormNock = nock(testUtil.webApiBaseUrl)
            .post('/api/v1/masterstyleforms/')
            .reply(200, function (uri, requestBody) {
                requestBody._id = 'id';
                return requestBody;
            });

        tokenManager.configureWebApiTokenManager(testUtil.webApiBaseUrl, 'token');
        var masterStyleFormClient = new clients.MasterStyleFormClient(tokenManager);
        var createdMasterStyle = await masterStyleFormClient.createMasterStyleForm(masterStyleForm);

        assert.equal(createdMasterStyle._id, 'id');
        assert.equal(createdMasterStyle.name, masterStyleForm.name);
        assert.equal(createdMasterStyle.deckId, masterStyleForm.deckId);
        assert.equal(createdMasterStyle.masterStyleId, masterStyleForm.masterStyleId);
    });

    it('should get masterStyleForms', async function () {
        var returnedMasterStyleForms = [
            {
                _id: '1234',
                name: 'testMSF',
                deckId: '1234',
                masterStyleId: '2345'
            },
            {
                _id: '3456',
                name: 'testMSF2',
                deckId: '4567',
                masterStyleId: '2345'
            }
        ];

        var getAllMasterStyleFormsNock = nock(testUtil.webApiBaseUrl)
            .get('/api/v1/masterstyleforms/')
            .reply(200, function (uri, requestBody) {
                return {
                    total: 2,
                    results: returnedMasterStyleForms
                };
            });

        var getOneMasterStyleFormNock = nock(testUtil.webApiBaseUrl)
            .get('/api/v1/masterstyleforms/?deckId=4567')
            .reply(200, function (uri, requestBody) {
                return {
                    total: 1,
                    results: [returnedMasterStyleForms[1]]
                };
            });

        tokenManager.configureWebApiTokenManager(testUtil.webApiBaseUrl, 'token');
        var masterStyleFormClient = new clients.MasterStyleFormClient(tokenManager);

        var results = await masterStyleFormClient.getMasterStyleForms({});

        assert.equal(results.total, 2);
        assert.equal(results.results[0]._id, returnedMasterStyleForms[0]._id);
        assert.equal(results.results[0].deckId, returnedMasterStyleForms[0].deckId);
        assert.equal(results.results[0].name, returnedMasterStyleForms[0].name);
        assert.equal(results.results[0].masterStyleId, returnedMasterStyleForms[0].masterStyleId);
        assert.equal(results.results[1]._id, returnedMasterStyleForms[1]._id);
        assert.equal(results.results[1].deckId, returnedMasterStyleForms[1].deckId);
        assert.equal(results.results[1].name, returnedMasterStyleForms[1].name);
        assert.equal(results.results[1].masterStyleId, returnedMasterStyleForms[1].masterStyleId);

        results = await masterStyleFormClient.getMasterStyleForms({ deckId: '4567' });

        assert.equal(results.total, 1);
        assert.equal(results.results[0]._id, returnedMasterStyleForms[1]._id);
        assert.equal(results.results[0].deckId, returnedMasterStyleForms[1].deckId);
        assert.equal(results.results[0].name, returnedMasterStyleForms[1].name);
        assert.equal(results.results[0].masterStyleId, returnedMasterStyleForms[1].masterStyleId);
    });

    it('should get only specified columns from masterStyleForms', async function () {
        var returnedMasterStyleForms = [
            {
                _id: '1234',
                name: 'testMSF',
                deckId: '1234',
                masterStyleId: '2345'
            },
            {
                _id: '3456',
                name: 'testMSF2',
                deckId: '4567',
                masterStyleId: '2345'
            }
        ];

        var getOneMasterStyleFormNock = nock(testUtil.webApiBaseUrl)
            .get('/api/v1/masterstyleforms?columns=name&deckId=4567')
            .reply(200, function (uri, requestBody) {
                return {
                    total: 1,
                    results: [
                        {
                            _id: returnedMasterStyleForms[1]._id,
                            name: returnedMasterStyleForms[1].name
                        }
                    ]
                };
            });

        tokenManager.configureWebApiTokenManager(testUtil.webApiBaseUrl, 'token');
        var masterStyleFormClient = new clients.MasterStyleFormClient(tokenManager);

        var results = await masterStyleFormClient.getMasterStyleForms({ deckId: '4567' }, 'name');

        assert.equal(results.total, 1);
        assert.equal(results.results[0]._id, returnedMasterStyleForms[1]._id);
        assert.equal(results.results[0].name, returnedMasterStyleForms[1].name);
    });
});

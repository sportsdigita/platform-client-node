const chai = require('chai');
const assert = chai.assert;
const nock = require('nock');
const deepCopy = require('deepcopy');
const tokenManager = new (require('../src/tokenManager'))();
const clients = require('../index');
const testUtil = require('./testUtil');

describe('fontClient', function () {
    this.afterEach(function () {
        nock.cleanAll();
    });

    it('should get fonts', async function () {
        returnedFonts = [
            {
                _id: '1234',
                deckId: '2345',
                name: 'font 1'
            },
            {
                _id: '3456',
                deckId: '4567',
                name: 'font 2'
            }
        ];

        var getAllFontsNock = nock(testUtil.webApiBaseUrl)
            .get('/api/v1/fonts')
            .reply(200, function (uri, requestBody) {
                return {
                    total: 2,
                    results: returnedFonts
                };
            });

        var getOneFontNock = nock(testUtil.webApiBaseUrl)
            .get('/api/v1/fonts?deckId=4567')
            .reply(200, function (uri, requestBody) {
                return {
                    total: 1,
                    results: [returnedFonts[1]]
                };
            });

        tokenManager.configureWebApiTokenManager(testUtil.webApiBaseUrl, 'token');
        var fontClient = new clients.FontClient(tokenManager);

        var results = await fontClient.getFonts({});

        assert.equal(results.total, 2);
        assert.equal(results.results[0]._id, returnedFonts[0]._id);
        assert.equal(results.results[0].deckId, returnedFonts[0].deckId);
        assert.equal(results.results[0].name, returnedFonts[0].name);
        assert.equal(results.results[1]._id, returnedFonts[1]._id);
        assert.equal(results.results[1].deckId, returnedFonts[1].deckId);
        assert.equal(results.results[1].name, returnedFonts[1].name);

        results = await fontClient.getFonts({ deckId: '4567' });

        assert.equal(results.total, 1);
        assert.equal(results.results[0]._id, returnedFonts[1]._id);
        assert.equal(results.results[0].deckId, returnedFonts[1].deckId);
        assert.equal(results.results[0].name, returnedFonts[1].name);
    });

    it('should get only specified columns from fonts', async function () {
        var returnedFonts = [
            {
                _id: '1234',
                deckId: '2345',
                name: 'font 1'
            },
            {
                _id: '3456',
                deckId: '4567',
                name: 'font 2'
            }
        ];

        var getOneFontNock = nock(testUtil.webApiBaseUrl)
            .get('/api/v1/fonts?columns=name&deckId=4567')
            .reply(200, function (uri, requestBody) {
                return {
                    total: 1,
                    results: [
                        {
                            _id: returnedFonts[1]._id,
                            name: returnedFonts[1].name
                        }
                    ]
                };
            });

        tokenManager.configureWebApiTokenManager(testUtil.webApiBaseUrl, 'token');
        var fontClient = new clients.FontClient(tokenManager);

        var results = await fontClient.getFonts({ deckId: '4567' }, 'name');

        assert.equal(results.total, 1);
        assert.equal(results.results[0]._id, returnedFonts[1]._id);
        assert.equal(results.results[0].name, returnedFonts[1].name);
    });

    it('should create a font', async function () {
        var fontRecord = {
            deckId: '4567',
            name: 'font 2'
        };

        var createFontNock = nock(testUtil.webApiBaseUrl)
            .post('/api/v1/fonts/')
            .reply(200, function (uri, requestBody) {
                requestBody._id = 'id';
                return requestBody;
            });

        tokenManager.configureWebApiTokenManager(testUtil.webApiBaseUrl, 'token');
        var fontClient = new clients.FontClient(tokenManager);

        var response = await fontClient.createFont(fontRecord);

        assert.equal(response._id, 'id');
        assert.equal(response.name, fontRecord.name);
        assert.equal(response.deckId, fontRecord.deckId);
    });

    it('should upload an font', async function () {
        var fontRecord = {
            deckId: '4567',
            name: 'font 2'
        };

        var createFontNock = nock(testUtil.webApiBaseUrl)
            .post('/api/v1/fonts/transcoder/')
            .reply(200, function (uri, requestBody) {
                var response = deepCopy(fontRecord);
                response._id = 'id';
                return response;
            });

        tokenManager.configureWebApiTokenManager(testUtil.webApiBaseUrl, 'token');
        var fontClient = new clients.FontClient(tokenManager);

        var fontPath = __dirname + '/./testFiles/testFont.woff';
        var response = await fontClient.uploadFont(fontPath, fontRecord);

        assert.equal(response._id, 'id');
        assert.equal(response.name, fontRecord.name);
        assert.equal(response.deckId, fontRecord.deckId);
    });
});

const chai = require('chai');
const assert = chai.assert;
const nock = require('nock');
const deepCopy = require('deepcopy');
const tokenManager = new (require('../src/tokenManager'))();
const clients = require('../index');
const testUtil = require('./testUtil');

describe('audioClient', function () {
    this.afterEach(function () {
        nock.cleanAll();
    });

    var audioRecord = {
        title: 'audioTitle',
        subdomain: 'redsox',
        itemId: 'itemId',
        itemType: 'digideck',
        mp3: '1'
    };

    it('should create an audio', async function () {
        var createAudioNock = nock(testUtil.webApiBaseUrl)
            .post('/api/v1/audios?preserveAuditData=false')
            .reply(200, function (uri, requestBody) {
                requestBody._id = 'id';
                return requestBody;
            });

        tokenManager.configureWebApiTokenManager(testUtil.webApiBaseUrl, 'token');
        var audioClient = new clients.AudioClient(tokenManager);

        var response = await audioClient.createAudio(audioRecord);

        assert.equal(response._id, 'id');
        assert.equal(response.title, audioRecord.title);
        assert.equal(response.subdomain, audioRecord.subdomain);
        assert.equal(response.itemId, audioRecord.itemId);
        assert.equal(response.itemType, audioRecord.itemType);
        assert.equal(response.mp3, audioRecord.mp3);
    });

    it('should upload an audio', async function () {
        var createAudioNock = nock(testUtil.webApiBaseUrl)
            .post('/api/v1/audios/transcoder/')
            .reply(200, function (uri, requestBody) {
                var response = deepCopy(audioRecord);
                response._id = 'id';
                return response;
            });

        tokenManager.configureWebApiTokenManager(testUtil.webApiBaseUrl, 'token');
        var audioClient = new clients.AudioClient(tokenManager);

        var audioPath = __dirname + '/./testFiles/testAudio.mp3';
        var response = await audioClient.uploadAudio(audioPath, audioRecord);

        assert.equal(response._id, 'id');
        assert.equal(response.title, audioRecord.title);
        assert.equal(response.subdomain, audioRecord.subdomain);
        assert.equal(response.itemId, audioRecord.itemId);
        assert.equal(response.itemType, audioRecord.itemType);
        assert.equal(response.mp3, audioRecord.mp3);
    });
});

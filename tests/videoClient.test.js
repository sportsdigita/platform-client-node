const chai = require('chai');
const assert = chai.assert;
const nock = require('nock');
const deepCopy = require('deepcopy');
const tokenManager = new (require('../src/tokenManager'))();
const clients = require('../index');
const testUtil = require('./testUtil');

describe('videoClient', function () {
    this.afterEach(function () {
        nock.cleanAll();
    });

    var videoRecord = {
        title: 'videoTitle',
        subdomain: 'redsox',
        itemId: 'itemId',
        itemType: 'digideck',
        mp4: '1'
    };

    it('should create an video', async function () {
        var createVideoNock = nock(testUtil.webApiBaseUrl)
            .post('/api/v1/videos?preserveAuditData=false')
            .reply(200, function (uri, requestBody) {
                requestBody._id = 'id';
                return requestBody;
            });

        tokenManager.configureWebApiTokenManager(testUtil.webApiBaseUrl, 'token');
        var videoClient = new clients.VideoClient(tokenManager);

        var response = await videoClient.createVideo(videoRecord);

        assert.equal(response._id, 'id');
        assert.equal(response.title, videoRecord.title);
        assert.equal(response.subdomain, videoRecord.subdomain);
        assert.equal(response.itemId, videoRecord.itemId);
        assert.equal(response.itemType, videoRecord.itemType);
        assert.equal(response.mp4, videoRecord.mp4);
    });

    it('should upload an video', async function () {
        var createVideoNock = nock(testUtil.webApiBaseUrl)
            .post('/api/v1/videos/transcoder/')
            .reply(200, function (uri, requestBody) {
                var response = deepCopy(videoRecord);
                response._id = 'id';
                return response;
            });

        tokenManager.configureWebApiTokenManager(testUtil.webApiBaseUrl, 'token');
        var videoClient = new clients.VideoClient(tokenManager);

        var videoPath = __dirname + '/./testFiles/testVideo.mp4';
        var response = await videoClient.uploadVideo(videoPath, videoRecord);

        assert.equal(response._id, 'id');
        assert.equal(response.title, videoRecord.title);
        assert.equal(response.subdomain, videoRecord.subdomain);
        assert.equal(response.itemId, videoRecord.itemId);
        assert.equal(response.itemType, videoRecord.itemType);
        assert.equal(response.mp4, videoRecord.mp4);
    });
});

const chai = require('chai');
const assert = chai.assert;
const nock = require('nock');
const deepCopy = require('deepcopy');
const tokenManager = new (require('../src/tokenManager'))();
const clients = require('../index');
const testUtil = require('./testUtil');

describe('fileClient', function () {
    this.afterEach(function () {
        nock.cleanAll();
    });

    it('should create a file record', async function () {
        var file = {
            subdomain: 'admin',
            itemType: 'presentation'
        };

        var getDeckNock = nock(testUtil.webApiBaseUrl)
            .post('/api/v1/files?preserveAuditData=false')
            .reply(200, function (uri, requestBody) {
                var response = deepCopy(file);
                response._id = 'id';
                return response;
            });

        tokenManager.configureWebApiTokenManager(testUtil.webApiBaseUrl, 'token');
        var fileClient = new clients.FileClient(tokenManager);
        var createdFile = await fileClient.createFile(file);

        assert.equal(createdFile._id, 'id');
        assert.equal(createdFile.subdomain, file.subdomain);
        assert.equal(createdFile.itemType, file.itemType);
    });
});

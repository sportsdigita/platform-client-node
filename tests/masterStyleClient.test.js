const chai = require('chai');
const assert = chai.assert;
const nock = require('nock');
const tokenManager = new (require('../src/tokenManager'))();
const clients = require('../index');
const testUtil = require('./testUtil');

describe('masterStyleClient', function () {
    this.afterEach(function () {
        nock.cleanAll();
    });

    it('should create a masterStyle', async function () {
        var masterStyle = {
            name: 'testMS',
            deckId: '1234',
            description: 'test desc.'
        };

        var createMasterStyleNock = nock(testUtil.webApiBaseUrl)
            .post('/api/v1/masterstyles/')
            .reply(200, function (uri, requestBody) {
                requestBody._id = 'id';
                return requestBody;
            });

        tokenManager.configureWebApiTokenManager(testUtil.webApiBaseUrl, 'token');
        var masterStyleClient = new clients.MasterStyleClient(tokenManager);
        var createdMasterStyle = await masterStyleClient.createMasterStyle(masterStyle);

        assert.equal(createdMasterStyle._id, 'id');
        assert.equal(createdMasterStyle.name, masterStyle.name);
        assert.equal(createdMasterStyle.deckId, masterStyle.deckId);
        assert.equal(createdMasterStyle.description, masterStyle.description);
    });
});

const chai = require('chai');
const assert = chai.assert;
const nock = require('nock');
const deepCopy = require('deepcopy');
const tokenManager = new (require('../src/tokenManager'))();
const clients = require('../index');
const testUtil = require('./testUtil');

describe('deckClient', function () {
    this.afterEach(function () {
        nock.cleanAll();
    });

    var deckRecord = {
        name: 'testDeck',
        organizationId: 'orgId',
        subdomain: 'test'
    };

    var deckSlideRecord = {
        deckId: 'deckId',
        content: 'asdfasdf',
        path: '/foo/bar',
        parentPath: '/foo',
        nestingLevel: -1
    };

    var deckId = '56251e64542b96182e8b7324';

    it('should get a deck', async function () {
        var getDeckNock = nock(testUtil.webApiBaseUrl)
            .get('/api/v1/decks/' + deckId)
            .reply(200, function (uri, requestBody) {
                var response = deepCopy(deckRecord);
                response._id = 'id';
                return response;
            });

        tokenManager.configureWebApiTokenManager(testUtil.webApiBaseUrl, 'token');
        var deckClient = new clients.DeckClient(tokenManager);
        var deck = await deckClient.getDeck(deckId);

        assert.equal(deck._id, 'id');
        assert.equal(deck.name, deckRecord.name);
        assert.equal(deck.organizationId, deckRecord.organizationId);
        assert.equal(deck.subdomain, deckRecord.subdomain);
    });

    it('should get the specified columns in a deck', async function () {
        var getDeckNock = nock(testUtil.webApiBaseUrl)
            .get('/api/v1/decks/' + deckId + '?columns=name')
            .reply(200, function (uri, requestBody) {
                return { _id: 'id', name: deckRecord.name };
            });

        tokenManager.configureWebApiTokenManager(testUtil.webApiBaseUrl, 'token');
        var deckClient = new clients.DeckClient(tokenManager);
        var deck = await deckClient.getDeck(deckId, 'name');

        assert.equal(deck._id, 'id');
        assert.equal(deck.name, deckRecord.name);
    });

    it('should get decks based on a filter', async function () {
        var getDeckNock = nock(testUtil.webApiBaseUrl)
            .get('/api/v1/decks' + '?columns=name&organizationId=12345')
            .reply(200, function (uri, requestBody) {
                return {
                    total: 2,
                    results: [
                        { _id: '23456', name: 'deck 1' },
                        { _id: '34567', name: 'deck 2' }
                    ]
                };
            });

        tokenManager.configureWebApiTokenManager(testUtil.webApiBaseUrl, 'token');
        var deckClient = new clients.DeckClient(tokenManager);
        var filter = { organizationId: '12345' };
        var deck = await deckClient.getDecks(filter, 'name');

        assert.equal(deck.total, 2);
        assert.equal(deck.results[0]._id, '23456');
        assert.equal(deck.results[0].name, 'deck 1');
        assert.equal(deck.results[1]._id, '34567');
        assert.equal(deck.results[1].name, 'deck 2');
    });

    it('should create deck slides', async function () {
        var slides = [
            {
                deckId: deckId,
                path: '/chapter/slide1',
                parentPath: '/chapter'
            },
            {
                deckId: deckId,
                path: '/chapter/slide2',
                parentPath: '/chapter'
            }
        ];

        var createDeckSlidesNock = nock(testUtil.webApiBaseUrl)
            .post('/api/v1/decks/' + deckId + '/slides')
            .reply(200, function (uri, requestBody) {
                var createdSlides = deepCopy(slides);
                createdSlides[0]._id = 'slide1Id';
                createdSlides[1]._id = 'slide2Id';
                return createdSlides;
            });

        tokenManager.configureWebApiTokenManager(testUtil.webApiBaseUrl, 'token');
        var deckClient = new clients.DeckClient(tokenManager);

        var response = await deckClient.createDeckSlides(deckId, slides);

        assert.equal(response.length, 2);
        assert.equal(response[0]._id, 'slide1Id');
        assert.equal(response[0].path, slides[0].path);
        assert.equal(response[0].parentPath, slides[0].parentPath);
        assert.equal(response[1]._id, 'slide2Id');
        assert.equal(response[1].path, slides[1].path);
        assert.equal(response[1].parentPath, slides[1].parentPath);
    });

    it('should get deck slides', async () => {
        var slides = [
            {
                _id: 'asdfasdfased',
                deckId: deckId,
                path: '/chapter/slide1',
                parentPath: '/chapter'
            },
            {
                _id: 'fooalkdjf',
                deckId: deckId,
                path: '/chapter/slide2',
                parentPath: '/chapter'
            }
        ];
        var getDeckSlidesNock = nock(testUtil.webApiBaseUrl)
            .get('/api/v1/decks/' + deckId + '/slides')
            .reply(200, function (uri, requestBody) {
                var createdSlides = deepCopy(slides);
                return { results: createdSlides, total: 2 };
            });

        tokenManager.configureWebApiTokenManager(testUtil.webApiBaseUrl, 'token');
        var deckClient = new clients.DeckClient(tokenManager);

        var response = await deckClient.getDeckSlides(deckId, {});

        assert.equal(response.total, 2);
        assert.equal(response.results.length, 2);
        assert.equal(response.results[0].path, slides[0].path);
        assert.equal(response.results[0]._id, slides[0]._id);
        assert.equal(response.results[0].parentPath, slides[0].parentPath);
        assert.equal(response.results[1]._id, slides[1]._id);
        assert.equal(response.results[1].path, slides[1].path);
        assert.equal(response.results[1].parentPath, slides[1].parentPath);
    });

    it('should update deck slides', async () => {
        var slides = [
            {
                _id: 'asdfasdfased',
                deckId: deckId,
                path: '/chapter/slide1',
                parentPath: '/chapter'
            },
            {
                _id: 'fooalkdjf',
                deckId: deckId,
                path: '/chapter/slide2',
                parentPath: '/chapter'
            }
        ];
        var updateDeckSlidesNock = nock(testUtil.webApiBaseUrl)
            .put('/api/v1/decks/' + deckId + '/slides', slides)
            .reply(200, function (uri, requestBody) {
                var createdSlides = deepCopy(slides);
                return createdSlides;
            });

        tokenManager.configureWebApiTokenManager(testUtil.webApiBaseUrl, 'token');
        var deckClient = new clients.DeckClient(tokenManager);

        var response = await deckClient.updateDeckSlides(deckId, slides);

        assert.equal(response.length, 2);
        assert.equal(response[0].path, slides[0].path);
        assert.equal(response[0]._id, slides[0]._id);
        assert.equal(response[0].parentPath, slides[0].parentPath);
        assert.equal(response[1]._id, slides[1]._id);
        assert.equal(response[1].path, slides[1].path);
        assert.equal(response[1].parentPath, slides[1].parentPath);
    });

    it('should update a deck slide', async () => {
        var slide = {
            _id: 'asdkfjasdkfj',
            deckId: deckId,
            path: '/chapter/slide1',
            parentPath: '/chapter'
        };
        var updateDeckSlideNock = nock(testUtil.webApiBaseUrl)
            .put('/api/v1/decks/' + deckId + '/slides/' + slide._id, slide)
            .reply(200, function (uri, requestBody) {
                const createdSlide = deepCopy(slide);
                return createdSlide;
            });

        tokenManager.configureWebApiTokenManager(testUtil.webApiBaseUrl, 'token');
        var deckClient = new clients.DeckClient(tokenManager);

        var response = await deckClient.updateDeckSlide(deckId, slide._id, slide);

        assert.equal(response.path, slide.path);
        assert.equal(response.deckId, slide.deckId);
        assert.equal(response._id, slide._id);
        assert.equal(response.parentPath, slide.parentPath);
    });

    it('should update deck', async function () {
        var updateNock = nock(testUtil.webApiBaseUrl)
            .put('/api/v1/decks/' + deckId)
            .reply(200, function (uri, requestBody) {
                var updatedDeck = deepCopy(deckRecord);
                updatedDeck._id = 'id';
                updatedDeck.isPublic = false;
                return updatedDeck;
            });

        tokenManager.configureWebApiTokenManager(testUtil.webApiBaseUrl, 'token');
        var deckClient = new clients.DeckClient(tokenManager);

        var response = await deckClient.updateDeck(deckId, { isPublic: false });

        assert.equal(response.isPublic, false);
        assert.equal(response._id, 'id');
        assert.equal(response.name, deckRecord.name);
        assert.equal(response.organizationId, deckRecord.organizationId);
        assert.equal(response.subdomain, deckRecord.subdomain);
    });

    it('should get vanity urls for all deck', async function () {
        var updateNock = nock(testUtil.webApiBaseUrl)
            .get('/api/v2/deckvanityurls')
            .reply(200, function (uri, requestBody) {
                return {
                    total: 20,
                    results: [{ vanityUrl: 'packers-activation.jensons.org', deckUrl: 'packers-activation.v5.dev.sportsdigita.com/' }]
                };
            });

        tokenManager.configureWebApiTokenManager(testUtil.webApiBaseUrl, 'token');
        var deckClient = new clients.DeckClient(tokenManager);

        var response = await deckClient.getVanityUrls({});

        assert.equal(response.total, 20);
        assert.equal(response.results[0].vanityUrl, 'packers-activation.jensons.org');
    });
});

const chai = require('chai');
const assert = chai.assert;
const nock = require('nock');
const tokenManager = new (require('../src/tokenManager'))();
const clients = require('../index');
const testUtil = require('./testUtil');

describe('masterStyleFormClient', function () {
    this.afterEach(function () {
        nock.cleanAll();
    });

    var returnedNotifications = [
        {
            _id: '1234',
            interval: 'daily',
            method: 'email',
            type: 'activity'
        },
        {
            _id: '3456',
            interval: 'weekly',
            method: 'email',
            type: 'activity'
        }
    ];

    it('should get notifications', async function () {
        var getAllNotificationsNock = nock(testUtil.webApiBaseUrl)
            .get('/api/v1/notifications')
            .reply(200, function (uri, requestBody) {
                return {
                    total: 2,
                    results: returnedNotifications
                };
            });

        var getOneNotificationNock = nock(testUtil.webApiBaseUrl)
            .get('/api/v1/notifications?interval=weekly')
            .reply(200, function (uri, requestBody) {
                return {
                    total: 1,
                    results: [returnedNotifications[1]]
                };
            });

        tokenManager.configureWebApiTokenManager(testUtil.webApiBaseUrl, 'token');
        var notificationClient = new clients.NotificationClient(tokenManager);

        var results = await notificationClient.getNotifications({});

        assert.equal(results.total, 2);
        assert.equal(results.results[0]._id, returnedNotifications[0]._id);
        assert.equal(results.results[0].method, returnedNotifications[0].method);
        assert.equal(results.results[0].interval, returnedNotifications[0].interval);
        assert.equal(results.results[0].type, returnedNotifications[0].type);
        assert.equal(results.results[1]._id, returnedNotifications[1]._id);
        assert.equal(results.results[1].method, returnedNotifications[1].method);
        assert.equal(results.results[1].interval, returnedNotifications[1].interval);
        assert.equal(results.results[1].type, returnedNotifications[1].type);

        results = await notificationClient.getNotifications({ interval: 'weekly' });

        assert.equal(results.total, 1);
        assert.equal(results.results[0]._id, returnedNotifications[1]._id);
        assert.equal(results.results[0].method, returnedNotifications[1].method);
        assert.equal(results.results[0].interval, returnedNotifications[1].interval);
        assert.equal(results.results[0].type, returnedNotifications[1].type);
    });

    it('should get only specified columns from notifications', async function () {
        var getOneMasterStyleFormNock = nock(testUtil.webApiBaseUrl)
            .get('/api/v1/notifications?columns=interval&interval=weekly')
            .reply(200, function (uri, requestBody) {
                return {
                    total: 1,
                    results: [
                        {
                            _id: returnedNotifications[1]._id,
                            interval: returnedNotifications[1].interval
                        }
                    ]
                };
            });

        tokenManager.configureWebApiTokenManager(testUtil.webApiBaseUrl, 'token');
        var notificationClient = new clients.NotificationClient(tokenManager);

        var results = await notificationClient.getNotifications({ interval: 'weekly' }, 'interval');

        assert.equal(results.total, 1);
        assert.equal(results.results[0]._id, returnedNotifications[1]._id);
        assert.equal(results.results[0].interval, returnedNotifications[1].interval);
    });
});

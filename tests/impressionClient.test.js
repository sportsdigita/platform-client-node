const chai = require('chai');
const assert = chai.assert;
const nock = require('nock');
const tokenManager = new (require('../src/tokenManager'))();
const clients = require('../index');
const testUtil = require('./testUtil');

describe('fontClient', function () {
    this.afterEach(function () {
        nock.cleanAll();
    });

    it('should get impression stats', async function () {
        var getImpressionStatsNock = nock(testUtil.webApiBaseUrl)
            .get('/api/v1/impressionstats/path/totalSeconds?presentationId=1234')
            .reply(200, function (uri, requestBody) {
                return [
                    {
                        totalSeconds: 100
                    }
                ];
            });

        tokenManager.configureWebApiTokenManager(testUtil.webApiBaseUrl, 'token');
        var impressionClient = new clients.ImpressionClient(tokenManager);

        var response = await impressionClient.getImpressionStats('path', 'totalSeconds', { presentationId: 1234 });

        assert.equal(response[0].totalSeconds, 100);
    });
});

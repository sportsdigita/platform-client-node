const chai = require('chai');
const assert = chai.assert;
const nock = require('nock');
const deepCopy = require('deepcopy');
const tokenManager = new (require('../src/tokenManager'))();
const clients = require('../index');
const testUtil = require('./testUtil');

describe('presentationClient', function () {
    this.afterEach(function () {
        nock.cleanAll();
    });

    var presentationRecord = {
        name: 'testPrez',
        deckId: '2345',
        title: 'testPres'
    };

    var presentationId = '56251e64542b96182e8b7324';

    it('should update a presentation', async function () {
        var updateNock = nock(testUtil.webApiBaseUrl)
            .put('/api/v1/presentations/' + presentationId)
            .reply(200, function (uri, requestBody) {
                var updatedPresentation = deepCopy(presentationRecord);
                updatedPresentation._id = presentationId;
                updatedPresentation.isPublic = false;
                return updatedPresentation;
            });

        tokenManager.configureWebApiTokenManager(testUtil.webApiBaseUrl, 'token');
        var presentationClient = new clients.PresentationClient(tokenManager);

        var response = await presentationClient.updatePresentation(presentationId, { isPublic: false });

        assert.equal(response.isPublic, false);
        assert.equal(response._id, presentationId);
        assert.equal(response.name, presentationRecord.name);
        assert.equal(response.deckId, presentationRecord.deckId);
        assert.equal(response.title, presentationRecord.title);
    });

    var returnedPresentations = [
        {
            _id: '1234',
            name: 'testPrez',
            deckId: '2345',
            title: 'testPres'
        },
        {
            _id: '3456',
            name: 'testPrez2',
            deckId: '4567',
            title: 'testPres2'
        }
    ];

    it('should get presentations', async function () {
        var getAllPresentationsNock = nock(testUtil.webApiBaseUrl)
            .get('/api/v1/presentations')
            .reply(200, function (uri, requestBody) {
                return {
                    total: 2,
                    results: returnedPresentations
                };
            });

        var getOnePresentationsNock = nock(testUtil.webApiBaseUrl)
            .get('/api/v1/presentations?deckId=4567')
            .reply(200, function (uri, requestBody) {
                return {
                    total: 1,
                    results: [returnedPresentations[1]]
                };
            });

        tokenManager.configureWebApiTokenManager(testUtil.webApiBaseUrl, 'token');
        var presentationClient = new clients.PresentationClient(tokenManager);

        var results = await presentationClient.getPresentations({});

        assert.equal(results.total, 2);
        assert.equal(results.results[0]._id, returnedPresentations[0]._id);
        assert.equal(results.results[0].deckId, returnedPresentations[0].deckId);
        assert.equal(results.results[0].name, returnedPresentations[0].name);
        assert.equal(results.results[0].title, returnedPresentations[0].title);
        assert.equal(results.results[1]._id, returnedPresentations[1]._id);
        assert.equal(results.results[1].deckId, returnedPresentations[1].deckId);
        assert.equal(results.results[1].name, returnedPresentations[1].name);
        assert.equal(results.results[1].title, returnedPresentations[1].title);

        results = await presentationClient.getPresentations({ deckId: '4567' });

        assert.equal(results.total, 1);
        assert.equal(results.results[0]._id, returnedPresentations[1]._id);
        assert.equal(results.results[0].deckId, returnedPresentations[1].deckId);
        assert.equal(results.results[0].name, returnedPresentations[1].name);
        assert.equal(results.results[0].title, returnedPresentations[1].title);
    });

    it('should get only specified columns from presentations', async function () {
        var getOnePresentationNock = nock(testUtil.webApiBaseUrl)
            .get('/api/v1/presentations?columns=name&deckId=4567')
            .reply(200, function (uri, requestBody) {
                return {
                    total: 1,
                    results: [
                        {
                            _id: returnedPresentations[1]._id,
                            name: returnedPresentations[1].name
                        }
                    ]
                };
            });

        tokenManager.configureWebApiTokenManager(testUtil.webApiBaseUrl, 'token');
        var presentationClient = new clients.PresentationClient(tokenManager);

        var results = await presentationClient.getPresentations({ deckId: '4567' }, 'name');

        assert.equal(results.total, 1);
        assert.equal(results.results[0]._id, returnedPresentations[1]._id);
        assert.equal(results.results[0].name, returnedPresentations[1].name);
    });

    it('should get presentation stats', async function () {
        var getPresentationStatsNock = nock(testUtil.webApiBaseUrl)
            .get('/api/v1/presentationstats/path/totalSeconds?presentationId=1234')
            .reply(200, function (uri, requestBody) {
                return [
                    {
                        totalSeconds: 100
                    }
                ];
            });

        tokenManager.configureWebApiTokenManager(testUtil.webApiBaseUrl, 'token');
        var presentationClient = new clients.PresentationClient(tokenManager);

        var response = await presentationClient.getPresentationStats('path', 'totalSeconds', { presentationId: 1234 });

        assert.equal(response[0].totalSeconds, 100);
    });

    it('should get presentation slides', async () => {
        var slides = [
            {
                _id: 'asdfasdfased',
                presentationId: presentationId,
                path: '/chapter/slide1',
                parentPath: '/chapter'
            },
            {
                _id: 'fooalkdjf',
                presentationId: presentationId,
                path: '/chapter/slide2',
                parentPath: '/chapter'
            }
        ];

        var getPresentationSlidesNock = nock(testUtil.webApiBaseUrl)
            .get('/api/v1/presentations/' + presentationId + '/slides')
            .reply(200, function (uri, requestBody) {
                var getSlides = deepCopy(slides);
                return { results: getSlides, total: 2 };
            });

        tokenManager.configureWebApiTokenManager(testUtil.webApiBaseUrl, 'token');
        var presentationClient = new clients.PresentationClient(tokenManager);

        var response = await presentationClient.getPresentationSlides(presentationId, {});

        assert.equal(response.total, 2);
        assert.equal(response.results.length, 2);
        assert.equal(response.results[0].path, slides[0].path);
        assert.equal(response.results[0]._id, slides[0]._id);
        assert.equal(response.results[0].parentPath, slides[0].parentPath);
        assert.equal(response.results[1]._id, slides[1]._id);
        assert.equal(response.results[1].path, slides[1].path);
        assert.equal(response.results[1].parentPath, slides[1].parentPath);
    });

    it('should update presentation slides', async () => {
        var slides = [
            {
                _id: 'asdfasdfased',
                presentationId: presentationId,
                path: '/chapter/slide1',
                parentPath: '/chapter'
            },
            {
                _id: 'fooalkdjf',
                presentationId: presentationId,
                path: '/chapter/slide2',
                parentPath: '/chapter'
            }
        ];

        var updatePresentationSlidesNock = nock(testUtil.webApiBaseUrl)
            .put('/api/v1/presentations/' + presentationId + '/slides', slides)
            .reply(200, function (uri, requestBody) {
                var createdSlides = deepCopy(slides);
                return createdSlides;
            });

        tokenManager.configureWebApiTokenManager(testUtil.webApiBaseUrl, 'token');
        var presentationClient = new clients.PresentationClient(tokenManager);

        var response = await presentationClient.updatePresentationSlides(presentationId, slides);

        assert.equal(response.length, 2);
        assert.equal(response[0].path, slides[0].path);
        assert.equal(response[0]._id, slides[0]._id);
        assert.equal(response[0].parentPath, slides[0].parentPath);
        assert.equal(response[1]._id, slides[1]._id);
        assert.equal(response[1].path, slides[1].path);
        assert.equal(response[1].parentPath, slides[1].parentPath);
    });

    it('should update a presentation slide', async () => {
        var slide = {
            _id: 'asdkfjasdkfj',
            presentationId: presentationId,
            path: '/chapter/slide1',
            parentPath: '/chapter'
        };
        var updatePresentationSlideNock = nock(testUtil.webApiBaseUrl)
            .put('/api/v1/presentations/' + presentationId + '/slides/' + slide._id, slide)
            .reply(200, function (uri, requestBody) {
                const createdSlide = deepCopy(slide);
                return createdSlide;
            });

        tokenManager.configureWebApiTokenManager(testUtil.webApiBaseUrl, 'token');
        var presentationClient = new clients.PresentationClient(tokenManager);

        var response = await presentationClient.updatePresentationSlide(presentationId, slide._id, slide);

        assert.equal(response.path, slide.path);
        assert.equal(response.presentationId, slide.presentationId);
        assert.equal(response._id, slide._id);
        assert.equal(response.parentPath, slide.parentPath);
    });
});
